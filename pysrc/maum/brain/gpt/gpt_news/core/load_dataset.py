import glob
import numpy as np
import os
import tensorflow as tf
import tqdm

def load_dataset(enc, path, combine):
    paths = []
    if os.path.isfile(path):
        # Simple file
        paths.append(path)
    elif os.path.isdir(path):
        # Directory
        for (dirpath, _, fnames) in os.walk(path):
            for fname in fnames:
                paths.append(os.path.join(dirpath, fname))
    else:
        # Assume glob
        paths = glob.glob(path)

    token_chunks = []
    raw_text = ''
    for path in tqdm.tqdm(paths):
        if path.endswith('.npz'):
            # Pre-encoded
            with np.load(path) as npz:
                for item in npz.files:
                    token_chunks.append(npz[item])
        else:
            # Plain text
            with open(path, 'r') as fp:
                raw_text += fp.read()

            if len(raw_text) >= combine:
                tokens = np.stack(enc.encode(raw_text))
                token_chunks.append(tokens)
                raw_text = ''
            else:
                raw_text += '<|endoftext|>'

    if raw_text:
        tokens = np.stack(enc.encode(raw_text))
        token_chunks.append(tokens)
    return token_chunks


def binary_search(f, lo, hi):
    if f(lo) or not f(hi):
        return None
    while hi > lo + 1:
        mid = (lo + hi) // 2
        if f(mid):
            hi = mid
        else:
            lo = mid
    return hi


class Sampler(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, chunks, seed=None):
        self.chunks = chunks
        self.total_size = sum(chunk.shape[0] for chunk in chunks)
        self.boundaries = [0]
        for i in range(len(chunks)):
            self.boundaries.append(self.boundaries[-1] + chunks[i].shape[0])
        self.rs = np.random.RandomState(seed=seed)

    def sample(self, length):
        assert length < self.total_size // len(
            self.chunks
        ), "Dataset files are too small to sample {} tokens at a time".format(
            length)
        while True:
            index = self.rs.randint(0, self.total_size - length - 1)
            i = binary_search(lambda j: self.boundaries[j] > index, 0,
                              len(self.boundaries) - 1) - 1
            if self.boundaries[i + 1] > index + length:
                within_chunk = index - self.boundaries[i]
                return self.chunks[i][within_chunk:within_chunk + length]

class Sampler_hdf5(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, chunks, seed=None):
        self.chunks = chunks
        self.total_size = sum(chunk.shape[0] for chunk in chunks)
        self.boundaries = [0]
        for i in range(len(chunks)):
            self.boundaries.append(self.boundaries[-1] + chunks[i].shape[0])
        self.rs = np.random.RandomState(seed=seed)

    def sample(self, length):
        # assert length < self.total_size // len(
        #     self.chunks
        # ), "Dataset files are too small to sample {} tokens at a time".format(
        #     length)
        while True:
            index = self.rs.randint(0, self.total_size - length - 1)
            i = binary_search(lambda j: self.boundaries[j] > index, 0,
                              len(self.boundaries) - 1) - 1
            if self.boundaries[i + 1] > index + length:
                within_chunk = index - self.boundaries[i]
                return self.chunks[i][within_chunk:within_chunk + length]


class Sampler_hdf52(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, chunks, seed=None):
        self.chunks = chunks
        self.total_size = sum(chunk.shape[0] for chunk in chunks)
        self.boundaries = [0]
        for i in range(len(chunks)):
            self.boundaries.append(self.boundaries[-1] + chunks[i].shape[0])
        self.rs = np.random.RandomState(seed=seed)

    def sample(self, length):
        assert length < self.total_size // len(
            self.chunks
        ), "Dataset files are too small to sample {} tokens at a time".format(
            length)
        while True:
            self.index = self.rs.randint(0, self.total_size - length - 1)
            self.i = binary_search(lambda j: self.boundaries[j] > self.index, 0,
                              len(self.boundaries) - 1) - 1
            if self.boundaries[self.i + 1] > self.index + length:
                self.within_chunk = index - self.boundaries[self.i]
                return self.chunks[self.i][self.within_chunk:self.within_chunk + length]


class Sampler_hdf5_per_news(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, chunks, seed=None):
        self.chunks = chunks
        self.chunks_len = len(chunks)

    def sample(self, length):

        while 1:
            self.selection = np.random.choice(self.chunks_len, 1)[0]
            if length <= len(self.chunks[self.selection]):
                break

        slected_text_len = len(self.chunks[self.selection])
        selecting_range = slected_text_len - length
        self.start_idx = np.random.choice(selecting_range+1, 1)[0]

        return self.chunks[self.selection][self.start_idx:self.start_idx+length]

class Sampler_hdf5_news(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, chunks, seed=None):
        self.chunks = chunks
        self.chunks_len = len(chunks)

    def sample(self, length, masking=True):

        self.selection = np.random.choice(self.chunks_len, 1)[0]

        slected_text_len = len(self.chunks[self.selection])
        selecting_range = slected_text_len - length

        if selecting_range >= 0:
            self.start_idx = np.random.choice(selecting_range+1, 1)[0]
            res = self.chunks[self.selection][self.start_idx:self.start_idx+length]
            mas = [1] * length
        else:
            origin_len = len(self.chunks[self.selection])
            res = list(self.chunks[self.selection]) + [0] * (length - origin_len)
            mas = ([1] * origin_len) + ([0] * (length - origin_len))

        if masking == True:
            return res, mas
        else:
            return res


'''
import h5py

f_path = '/DATA1/gpt-2/news_category.hdf5'
fi = h5py.File(f_path, 'r')
fi['category']
fi['category']
fi['category']['1'][9]
'''


class Sampler_hdf5_korean(object):
    """Fairly samples a slice from a set of variable sized chunks.

    'Fairly' means that the distribution is the same as sampling from one concatenated chunk,
    but without crossing chunk boundaries."""

    def __init__(self, group, total_size=None, boundary=None, seed=None):

        if total_size != None:
            self.group = group
            self.dataset = []
            self.dataset_total_size = total_size
            self.dataset_boundaries = boundary
            for int_name in range(len(group)):
                name_dataset = str(int_name)
                self.dataset.append(group[name_dataset])

        else:
            self.dataset = []
            self.dataset_total_size = []
            self.dataset_boundaries = []
            for int_name in tqdm.tqdm(range(len(group))):
                name_dataset = str(int_name)
                self.dataset.append(group[name_dataset])
                self.dataset_total_size.append(sum(chunk.shape[0] for chunk in group[name_dataset]))
                for i in range(len(group[name_dataset])):
                    if i == 0:
                        boundaries = [0]
                    boundaries.append(boundaries[-1] + group[name_dataset][i].shape[0])
                self.dataset_boundaries.append(boundaries)

        self.rs = np.random.RandomState(seed=seed)

    def sample(self, length):
        selected_category = np.random.choice(len(self.group), 1)[0]
        index = self.rs.randint(0, self.dataset_total_size[selected_category] - length - 1)
        i = binary_search(lambda j: self.dataset_boundaries[selected_category][j] > index, 0,
                          len(self.dataset_boundaries[selected_category]) - 1) - 1

        within_chunk = index - self.dataset_boundaries[selected_category][i]
        return_list = []
        counter_ = 0
        while 1:
            if counter_ == 0:
                return_list.extend(self.dataset[selected_category][i][within_chunk:])
            else:
                return_list.extend(self.dataset[selected_category][i])
            i += 1
            if len(return_list) >= length:
                break

            counter_ += 1

        return np.array(return_list[:length])

'''
import pickle
sampler = Sampler_hdf5_korean(fi['category'])
with open('cnn_total_size.pickle', 'wb') as f:
    pickle.dump(sampler.dataset_total_size, f)

with open('cnn_boundary.pickle', 'wb') as f:
    pickle.dump(sampler.dataset_boundaries, f)
'''
