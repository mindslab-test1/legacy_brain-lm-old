import numpy
import re


def convert_text(text):
   '''
   convert text for sentence split
   split token: '.. '
   '''
   out = re.sub('[\n]+', '',
                re.sub('[?]', '?.. ',
                       re.sub('다\.', '다... ',
                              re.sub('\."', '"',
                                     re.sub('[!]', '!.. ',
                                            re.sub('[.?]+', '.',
                                                   re.sub('[ ]+', ' ',
                                                          re.sub('[.]+', '.', text))))))))
   return out


def rm_sp(x):
    while 1:
        try:
            x.remove('')
        except:
            try:
                x.remove(' ')
            except:
                break
    return x


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = numpy.exp(x - numpy.max(x))
    return e_x / e_x.sum(axis=0)


def tokenizer_encode(passage, tokenizer):

        def is_whitespace(c):
            if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
                return True
            return False

        doc_tokens = []
        char_to_word_offset = []
        prev_is_whitespace = True
        for c in passage:
            if is_whitespace(c):
                prev_is_whitespace = True
                doc_tokens.append(' ')
            else:
                if prev_is_whitespace:
                    doc_tokens.append(c)
                else:
                    doc_tokens[-1] += c
                prev_is_whitespace = False
            char_to_word_offset.append(len(doc_tokens) - 1)

        tok_to_orig_index = []
        orig_to_tok_index = []
        sub_tokens_length = []
        all_doc_tokens = []
        for (i, token) in enumerate(doc_tokens):
            orig_to_tok_index.append(len(all_doc_tokens))
            sub_tokens = tokenizer.tokenize(token)
            if '[UNK]' in sub_tokens:
                sub_tokens = [token]
            if sub_tokens == []:
                sub_tokens_length[-1] += 1
            else:
                for sub_token in sub_tokens:
                    tok_to_orig_index.append(i)
                    sub_tokens_length.append(len(sub_token.replace(' ##', '').replace('##', '')))
                    all_doc_tokens.append(sub_token)

        return all_doc_tokens, sub_tokens_length
