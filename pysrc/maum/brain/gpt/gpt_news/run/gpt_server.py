from concurrent import futures

import logging
import time
import grpc
import json
import argparse
import sys

from gpt_inference import GPT as gpt

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

import gpt_pb2
from gpt_pb2_grpc import GptTextGenServicer
from gpt_pb2_grpc import add_GptTextGenServicer_to_server


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class GPT(GptTextGenServicer):
    def __init__(self, lang, device, sample):
        if lang == 'en':
            self.model = gpt(model_path='/home/msl/brain-lm/pysrc/maum/brain/gpt/gpt_news/gpt-eng', 
                             device=device, sample_=sample)
        else:
            self.model = gpt(model_path='/home/msl/brain-lm/pysrc/maum/brain/gpt/gpt_news/gpt-kor', 
                             device=device, sample_=sample)

    def Gen(self, msg, context):

        out_ = self.model.infer(msg.context)
        output = gpt_pb2.Outputs(gentext=out_[0])

        return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gpt runner executor')
    print(parser.description)
    parser.add_argument('-l', '--lang',
                        type=str)
    parser.add_argument('-d', '--device',
                        type=int, default=1)
    parser.add_argument('-s', '--sample',
                        type=int, default=1)
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=36001)

    args = parser.parse_args()

    gpt_ = GPT(args.lang, args.device, args.sample)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_GptTextGenServicer_to_server(gpt_, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('gpt starting at 0.0.0.0:%d' % args.port)
    print('gpt starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


