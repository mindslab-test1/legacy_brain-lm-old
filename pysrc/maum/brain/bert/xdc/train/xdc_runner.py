import os
import re
import sys
import json
import random
import argparse
import numpy as np
import tensorflow as tf
from tensorflow.python.client import device_lib
from datetime import datetime

sys.path.append('/home/minds/brain-lm/pysrc/maum/brain/bert/xdc')

from core import modeling
from core import tokenization
from core import xdc_core
from core.utils import convert_text, rm_sp


def get_available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


class BertXDC:
    def __init__(self, config, num_gpu):
        tf.logging.set_verbosity(tf.logging.INFO)
        self.config = config
        self.dir = os.environ['MAUM_ROOT']
        self.num_gpu = num_gpu

    def runner(self, train_batch_size, max_seq_length, num_train_epochs, train, en,
               x_attn, cnn_attn, max_sent_len, model_name):
        if not en:
            config = self.config['ko']
        else:
            config = self.config['en']

        bert_config = modeling.BertConfig.from_json_file(config['bert_config'])

        if config['max_seq_length'] > bert_config.max_position_embeddings:
            raise ValueError(
                "Cannot use sequence length %d because the BERT model "
                "was only trained up to sequence length %d" %
                (config['max_seq_length'], bert_config.max_position_embeddings))

        processor = xdc_core.XdcProcessor()

        tokenizer = tokenization.FullTokenizer(
            vocab_file=self.dir+config['vocab_file'],
            do_lower_case=en)

        train_examples = None
        num_train_steps = None
        num_warmup_steps = None
        if train:
            if model_name is None:
                model_name = re.sub(r'[-, :]', '', str(datetime.now())).split('.')[0] + '/'
            else:
                model_name = model_name + '/'

            tf.gfile.MakeDirs(self.dir+config['output_dir']+model_name)

            sess_config = tf.ConfigProto(
                gpu_options=tf.GPUOptions(allow_growth=True),
                allow_soft_placement=True)

            distribution = tf.contrib.distribute.MirroredStrategy(num_gpus=self.num_gpu, auto_shard_dataset=True)

            run_config = tf.contrib.tpu.RunConfig(
                master=None,
                save_summary_steps=100,
                log_step_count_steps=100,
                model_dir=self.dir+config['output_dir']+model_name,
                save_checkpoints_steps=config['save_checkpoints_steps'],
                train_distribute=distribution,
                session_config=sess_config)

            train_labels = processor.get_labels(self.dir + config['train_y_file'])
            train_examples = processor.get_train_examples(self.dir + config['train_x_file'], train_labels)

            num_train_steps = int(len(train_examples) / (train_batch_size * self.num_gpu) * num_train_epochs)
            num_warmup_steps = int(num_train_steps * config['warmup_proportion'])

            rng = random.Random(12345)
            rng.shuffle(train_examples)
        elif not train:
            if model_name is None:
                raise ValueError('Give me a model_mane, Plz!')
            else:
                model_name = model_name + '/'

            tf.gfile.MakeDirs(self.dir+config['output_dir'])

            sess_config = tf.ConfigProto(
                gpu_options=tf.GPUOptions(allow_growth=True),
                allow_soft_placement=True)

            run_config = tf.contrib.tpu.RunConfig(
                master=None,
                save_summary_steps=100,
                log_step_count_steps=100,
                model_dir=self.dir+config['output_dir']+model_name,
                save_checkpoints_steps=config['save_checkpoints_steps'],
                session_config=sess_config)

        model_fn = xdc_core.model_fn_builder(
            bert_config=bert_config,
            num_labels=54,
            init_checkpoint=self.dir+config['init_checkpoint'],
            learning_rate=config['learning_rate'],
            num_train_steps=num_train_steps,
            num_warmup_steps=num_warmup_steps,
            vocab_size=len(open(self.dir+config['vocab_file']).read().split('\n')) - 1,
            x_attn=x_attn,
            cnn_attn=cnn_attn,
            eval_batch=config['predict_batch_size'])

        estimator = tf.estimator.Estimator(
            model_fn=model_fn,
            config=run_config,
            params=dict(batch_size=train_batch_size,
                        eval_batch_size=config['predict_batch_size']))

        if train:
            tf.logging.info("***** Running training *****")
            tf.logging.info("  Num orig examples = %d", len(train_examples))

            train_data_dir = self.dir+config['vocab_file'][:-4]+'_train_data/'+\
                             config['train_x_file'].split('/')[-1][:-4]+'/'
            tf.gfile.MakeDirs(train_data_dir)

            if "train.tf_record" not in os.listdir(train_data_dir):
                xdc_core.file_based_convert_examples_to_features(
                    train_examples, max_seq_length, tokenizer,
                    os.path.join(train_data_dir, "train.tf_record"),
                    cnn_attn, max_sent_len)

            tf.logging.info("  Num examples = %d", len(train_examples))
            tf.logging.info("  Batch size = %d", train_batch_size)
            tf.logging.info("  Num steps = %d", num_train_steps)
            del train_examples

            train_input_fn = xdc_core.file_based_input_fn_builder(
                input_file=train_data_dir+"train.tf_record",
                seq_length=max_seq_length,
                is_training=train,
                drop_remainder=train,
                cnn_attn=cnn_attn,
                sent_len=max_sent_len)

            if config['eval_x_file'] is not None:
                eval_labels = processor.get_labels(self.dir + config['eval_y_file'])
                eval_examples = processor.get_dev_examples(self.dir + config['eval_x_file'], eval_labels)
                num_actual_eval_examples = len(eval_examples)

                if "eval.tf_record" not in os.listdir(train_data_dir):
                    xdc_core.file_based_convert_examples_to_features(
                        eval_examples, max_seq_length, tokenizer, os.path.join(train_data_dir, "eval.tf_record"),
                        cnn_attn, max_sent_len)

                tf.logging.info("***** Running evaluation *****")
                tf.logging.info("  Num examples = %d (%d actual, %d padding)",
                                len(eval_examples), num_actual_eval_examples,
                                len(eval_examples) - num_actual_eval_examples)
                tf.logging.info("  Batch size = %d", config['eval_batch_size'])

                # This tells the estimator to run through the entire set.

                del eval_examples

                predict_input_fn = xdc_core.file_based_input_fn_builder(
                    input_file=train_data_dir + "eval.tf_record",
                    seq_length=max_seq_length,
                    is_training=False,
                    drop_remainder=False,
                    cnn_attn=cnn_attn,
                    sent_len=max_sent_len)

                train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=num_train_steps)
                eval_spec = tf.estimator.EvalSpec(input_fn=predict_input_fn, throttle_secs=120)
                tf.estimator.train_and_evaluate(
                    estimator,
                    train_spec,
                    eval_spec)
            else:
                estimator.train(input_fn=train_input_fn, max_steps=num_train_steps)
        elif not train:
            dev_data_dir = self.dir+config['vocab_file'][:-4]+'_dev_data/'+\
                           config['predict_x_file'].split('/')[-1][:-4]+'/'
            tf.gfile.MakeDirs(dev_data_dir)

            model_step = estimator.get_variable_value("global_step")

            predict_labels = processor.get_labels(self.dir + config['predict_y_file'])
            predict_examples = processor.get_test_examples(self.dir + config['predict_x_file'], predict_labels)
            num_actual_predict_examples = len(predict_examples)

            predict_file = os.path.join(dev_data_dir, "predict.tf_record")
            if "predict.tf_record" not in os.listdir(dev_data_dir):
                xdc_core.file_based_convert_examples_to_features(predict_examples, max_seq_length, tokenizer,
                                                                 predict_file, cnn_attn, max_sent_len)

            tf.logging.info("***** Running prediction*****")
            tf.logging.info("  Num examples = %d (%d actual, %d padding)",
                            len(predict_examples), num_actual_predict_examples,
                            len(predict_examples) - num_actual_predict_examples)
            tf.logging.info("  Batch size = %d", config['predict_batch_size'])

            predict_input_fn = xdc_core.file_based_input_fn_builder(
                input_file=predict_file,
                seq_length=max_seq_length,
                is_training=train,
                drop_remainder=True,
                cnn_attn=cnn_attn,
                sent_len=max_sent_len)

            result = estimator.predict(input_fn=predict_input_fn, yield_single_examples=True)

            output_predict_file = os.path.join(self.dir + config['output_dir'], "%i_%s.tsv" % (model_step, model_name))

            with tf.gfile.GFile(output_predict_file, "w") as writer:
                num_written_lines = 0
                tf.logging.info("***** Predict results *****")
                score = 0
                for (i, prediction) in enumerate(result):
                    probabilities = prediction["probabilities"]
                    pred = np.argmax(probabilities)
                    label = prediction['labels']
                    score += int(np.equal(pred, label))
                    writer.write("label: %i, pred: %i\n" % (label, pred))
                    if cnn_attn:
                        word_attn = prediction['word_attention']
                        word_tok = tokenizer.convert_ids_to_tokens(prediction['input_ids'][1:])
                        top_k_words = [(i, word_tok[i], word_attn[i]) for i in (-1*np.array(word_attn)).argsort()[:10]]
                        sent_attn = prediction['sent_attention']
                        sents = [sent.strip() for sent in rm_sp(convert_text(predict_examples[i].text_a).split('.. '))]
                        pad_sents = ['[PAD]']*(max_sent_len-len(sents))
                        sents = sents + pad_sents
                        top_k_sents = [(i, sents[i], sent_attn[i]) for i in (-1*np.array(sent_attn)).argsort()[:2]]
                        writer.write("top_k_sents: %s\ntop_k_words: %s\n" % (str(top_k_sents), str(top_k_words)))
                    elif x_attn:
                        slen = 0
                        sents = []
                        sent_len = [0]
                        for sent in rm_sp(convert_text(predict_examples[i].text_a).split('.. ')):
                            sents.append(sent.strip())
                            slen += len(tokenizer.tokenize(sent))
                            sent_len.append(slen)
                        sent_attn = [np.mean(
                            prediction['word_attention'][sent_len[sent_len_idx]:sent_len[sent_len_idx+1]])
                            for sent_len_idx in range(len(sent_len)-1)]
                        top_k_sents = [(i, sents[i], sent_attn[i])for i in (-1*np.array(sent_attn)).argsort()[:2]]
                        word_attn = prediction['word_attention'][:sent_len[-1]]
                        word_tok = tokenizer.convert_ids_to_tokens(prediction['input_ids'][1:])[:sent_len[-1]]
                        top_k_words = [(i, word_tok[i], word_attn[i]) for i in (-1*np.array(word_attn)).argsort()[:10]]
                        writer.write("top_k_sents: %s\ntop_k_words: %s\n" % (str(top_k_sents), str(top_k_words)))
                    num_written_lines += 1
                writer.write("Acc: %.4f" % float(score/num_written_lines))
            print("Acc: %.4f" % float(score/num_written_lines))
            assert num_written_lines == num_actual_predict_examples


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--train_batch_size', type=int, default=6)
    parser.add_argument('-l', '--max_seq_length', type=int, default=384)
    parser.add_argument('-i', '--num_train_epochs', type=float, default=5)
    parser.add_argument('-t', '--train', type=bool, default=False)
    parser.add_argument('-e', '--en', type=bool, default=False)
    parser.add_argument('-x', '--x_attn', type=bool, default=False)
    parser.add_argument('-c', '--cnn_attn', type=bool, default=True)
    parser.add_argument('-sl', '--max_sent_len', type=int, default=30)
    parser.add_argument('-m', '--model_name', type=str, default=None)
    args = parser.parse_args()
    
    if args.en:
        lang = 'en'
    else:
        lang = 'ko'
    
    config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/xdc_config.json', 'r'))[lang]
    model = BertXDC(config, num_gpu=len(get_available_gpus()))

    model.runner(args.train_batch_size, args.max_seq_length, args.num_train_epochs, args.train, args.en,
                 args.x_attn, args.cnn_attn, args.max_sent_len, args.model_name)
