from concurrent import futures

import argparse
import logging
import time
import grpc
import json
import sys
import os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.xdc import bx_pb2
from maum.brain.bert.xdc.bx_pb2_grpc import BertXDCServicer
from maum.brain.bert.xdc.bx_pb2_grpc import add_BertXDCServicer_to_server
from maum.brain.bert.xdc.run.xdc_inference import BertXDC as BX

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class BertXDC(BertXDCServicer):
    def __init__(self, lang, model_name):
        self.config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/xdc_config.json', 'r'))
        self.model = BX(self.config, lang, model_name)

    def Answer(self, msg, context):

        answers = self.model.infer(msg.context)

        cls_outputs = []
        word_attn_outputs = []
        sent_attn_outputs = []
        for cls in answers[0][0]:
            cls_output = bx_pb2.Classify()
            cls_output.label = cls[0]
            cls_output.prob = cls[1]
            cls_outputs.append(cls_output)
        for word_attn in answers[0][1]:
            word_attn_output = bx_pb2.Attention()
            word_attn_output.start = word_attn[0][0]
            word_attn_output.end = word_attn[0][1]
            word_attn_output.weight = word_attn[-1]
            word_attn_outputs.append(word_attn_output)
        for sent_attn in answers[0][2]:
            sent_attn_output = bx_pb2.Attention()
            sent_attn_output.start = sent_attn[0][0]
            sent_attn_output.end = sent_attn[0][1]
            sent_attn_output.weight = sent_attn[-1]
            sent_attn_outputs.append(sent_attn_output)

        output = bx_pb2.Outputs()
        output.labels.extend(cls_outputs)
        output.word_idxes.extend(word_attn_outputs)
        output.sent_idxes.extend(sent_attn_outputs)

        return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='bertxdc runner executor')
    print(parser.description)
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=35002)
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='ko')
    parser.add_argument('-m', '--model',
                        type=str,
                        default='best')

    args = parser.parse_args()

    bertsqaud = BertXDC(args.lang, args.model)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_BertXDCServicer_to_server(bertsqaud, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('bertxdc starting at 0.0.0.0:%d' % args.port)
    print('bertxdc starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


