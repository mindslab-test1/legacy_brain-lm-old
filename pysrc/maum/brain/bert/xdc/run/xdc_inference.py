import tensorflow as tf
import numpy as np
import collections
import json
import os
import re

from ..core import modeling
from ..core import tokenization
from ..core import xdc_core
from ..core.utils import convert_text, rm_sp, softmax, tokenizer_encode


class BertXDC:
    def __init__(self, config, lang, init_checkpoint=None, device=None, is_training=False):
        if device is None:
            device = 0
        os.environ['CUDA_VISIBLE_DEVICES'] = str(device)

        if type(config) == str:
            config = json.load(open(config))[lang]
        else:
            config = config[lang]

        self.config = config
        self.dir = os.environ['MAUM_ROOT']
        self.is_training = is_training
        self.bert_config = modeling.BertConfig.from_json_file(config['bert_config'])

        self.num_labels = config['num_labels']
        self.label_dict = json.load(open(self.dir+config['label_dict'], encoding='utf8'))

        print('loaded ckpt graph')
        self.input_ids = tf.placeholder(tf.int32, shape=[None, config['max_seq_length']], name='input_ids')
        self.input_mask = tf.placeholder(tf.int32, shape=[None, config['max_seq_length']], name='input_mask')
        self.segment_ids = tf.placeholder(tf.int32, shape=[None, config['max_seq_length']], name='segment_ids')
        self.sent_len = tf.placeholder(tf.int32, shape=[None, config['max_sent_len']], name='sent_len')

        self.model_out = xdc_core.create_model(
            self.bert_config, is_training, self.input_ids, self.input_mask, self.segment_ids, self.num_labels,
            use_one_hot_embeddings=False,
            x_attn=config['x_attn'],
            cnn_attn=config['cnn_attn'],
            sent_len=self.sent_len,
            eval_batch=config['predict_batch_size'],
            vocab_size=len(open(self.dir+config['vocab_file']).read().split('\n')) - 1)

        tvars = tf.trainable_variables()

        initialized_variable_names = {}
        if init_checkpoint is None:
            init_checkpoint = config['init_checkpoint']
        if init_checkpoint:
            (assignment_map, initialized_variable_names) = \
                modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
            tf.train.init_from_checkpoint(init_checkpoint, assignment_map)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        self.sess.run(tf.global_variables_initializer())
        self.sess.run(self.model_out,
                      feed_dict={self.input_ids: [[0 for _ in range(self.input_ids.shape[-1])]],
                                 self.input_mask: [[0 for _ in range(self.input_mask.shape[-1])]],
                                 self.segment_ids: [[0 for _ in range(self.segment_ids.shape[-1])]],
                                 self.sent_len: [[0 for _ in range(self.sent_len.shape[-1])]]})
        
        tf.gfile.MakeDirs(self.dir + config['output_dir'])
        self.tokenizer = tokenization.FullTokenizer(vocab_file=self.dir + config['vocab_file'],
                                                    do_lower_case=config['do_lower_case'])

    def infer(self, input_doc):
        processor = xdc_core.XdcProcessor()
        examples = processor._create_examples([[input_doc]], set_type='perd', labels=None)

        eval_features = []
        for (ex_index, example) in enumerate(examples):
            feature = xdc_core.convert_single_example(
                ex_index, example, self.config['max_seq_length'], self.tokenizer,
                self.config['cnn_attn'], self.config['max_sent_len'])

            features = collections.OrderedDict()
            features["input_ids"] = feature.input_ids
            features["input_mask"] = feature.input_mask
            features["segment_ids"] = feature.segment_ids
            features["is_real_example"] = [int(feature.is_real_example)]
            try:
                features["sent_len"] = feature.sent_len
            except:
                features["sent_len"] = None
            eval_features.append(features)

        feed_dict = {self.input_ids: [features['input_ids'] for features in eval_features],
                     self.input_mask: [features['input_mask'] for features in eval_features],
                     self.segment_ids: [features['segment_ids'] for features in eval_features],
                     self.sent_len: [features['sent_len'] for features in eval_features]}
        result = self.sess.run(self.model_out, feed_dict=feed_dict)

        predictions = [[i[idx] for i in result]for idx in range(self.config['predict_batch_size'])]

        convert_pred = []
        output_predict_file = os.path.join(self.dir+self.config['output_dir'], "xdc_prediction.tsv")
        with tf.gfile.GFile(output_predict_file, "w") as writer:
            num_written_lines = 0
            tf.logging.info("***** Predict results *****")
            for (i, prediction) in enumerate(predictions):
                probabilities = prediction[1]
                preds = [(self.label_dict[str(i)], float(probabilities[i]))
                         for i in (-1 * np.array(probabilities)).argsort()[:self.config['num_top_label']]]
                writer.write("pred: %s\n" % str(preds))
                origin_context = examples[i].text_a
                if self.config['cnn_attn']:
                    word_attn = prediction[2]
                    # word_attn = softmax(word_attn*10000)
                    word_tok, word_tok_len = tokenizer_encode(origin_context, self.tokenizer)

                    # top_k_words_idx = (-1 * np.array(word_attn)).argsort()[:self.config['num_top_word']]
                    top_k_words_idx = []
                    for i in (-1 * np.array(word_attn)).argsort():
                        if len(word_tok[i].replace('##', '')) > 1:# and re.match(r'[a-zA-Z0-9]', word_tok[i].replace('##', '')) is None:
                            top_k_words_idx.append(i)
                        if len(top_k_words_idx) == self.config['num_top_word']:
                            break
                    # top_k_words_info = np.array(
                    #     [([sum(word_tok_len[:i]), sum(word_tok_len[:i]) + len(word_tok[i].replace('##', ''))],
                    #       word_tok[i], float(word_attn[i])) for i in sorted((-1 * np.array(word_attn)).argsort()
                    #     [:self.config['num_top_word']])]).tolist()
                    top_k_words_info = np.array(
                        [([sum(word_tok_len[:i]), sum(word_tok_len[:i]) + len(word_tok[i].replace('##', ''))],
                          word_tok[i], float(word_attn[i])) if '##' not in word_tok[i]
                         else ([sum(word_tok_len[:i-1]), sum(word_tok_len[:i-1]) +
                                len(''.join([word_tok[i-1], word_tok[i]]).replace('##', ''))],
                               ''.join([word_tok[i-1], word_tok[i]]).replace('##', ''), float(word_attn[i]))
                         for i in sorted(top_k_words_idx)]).tolist()

                    top_k_words_info_2 = []
                    s_s = 0
                    for out_i in range(len(top_k_words_info)):
                        if s_s == 1:
                            s_s = 0
                            continue
                        try:
                            tt = top_k_words_info[out_i + 1]

                            if (top_k_words_info[out_i][0][1] - top_k_words_info[out_i + 1][0][0]) > 0:
                                new_idx = [top_k_words_info[out_i][0][0], top_k_words_info[out_i + 1][0][1]]
                                new_w = origin_context[new_idx[0]:new_idx[1]]
                                new_weight = max([top_k_words_info[out_i][2], top_k_words_info[out_i + 1][2]])
                                top_k_words_info_2.append([new_idx, new_w, new_weight])
                                s_s = 1
                            else:
                                top_k_words_info_2.append(top_k_words_info[out_i])
                        except:
                            top_k_words_info_2.append(top_k_words_info[out_i])
                    top_k_words_info = top_k_words_info_2

                    sent_attn = prediction[4]
                    # sent_attn = softmax(softmax(sent_attn*10000)*100)
                    sents_info = np.array(
                        [(sent.strip(), len(sent)) for sent in rm_sp(convert_text(origin_context).split('.. '))]).T
                    sents = sents_info[0].tolist()
                    sents_len = [0]+[int(i) for i in sents_info[1]]
                    pad_sents = ['[PAD]'] * (self.config['max_sent_len'] - len(sents))
                    sents = sents + pad_sents
                    if len(sents_len) > 2:
                        top_k_sents_info = np.array(
                            [([sum([sents_len[j] for j in range(0, i+1)]), sum([sents_len[j] for j in range(0, i+2)])],
                              sents[i], float(sent_attn[i]))
                             for i in sorted((-1 * np.array(sent_attn)).argsort()[:self.config['num_top_sent']])]).tolist()
                    else:
                        top_k_sents_info = [([sum([sents_len[j] for j in range(0, 1)]),
                                              sum([sents_len[j] for j in range(0, 2)])], sents[0], float(sent_attn[0]))]
                    writer.write("top_k_sents: %s\ntop_k_words: %s\n" % (str(top_k_sents_info), str(top_k_words_info)))
                elif self.config['x_attn']:
                    slen = 0
                    sents = []
                    sent_len = [0]
                    for sent in rm_sp(convert_text(examples[i].text_a).split('.. ')):
                        sents.append(sent.strip())
                        slen += len(self.tokenizer.tokenize(sent))
                        sent_len.append(slen)
                    sent_attn = [np.mean(
                        prediction[2][sent_len[sent_len_idx]:sent_len[sent_len_idx + 1]])
                        for sent_len_idx in range(len(sent_len) - 1)]
                    top_k_sents = [(i, sents[i], sent_attn[i]) for i in (-1 * np.array(sent_attn)).argsort()[:2]]
                    word_attn = prediction[2][:sent_len[-1]]
                    word_tok = self.tokenizer.convert_ids_to_tokens(prediction[3][1:])[:sent_len[-1]]
                    top_k_words = [(i, word_tok[i], word_attn[i]) for i in (-1 * np.array(word_attn)).argsort()[:10]]
                    writer.write("top_k_sents: %s\ntop_k_words: %s\n" % (str(top_k_sents), str(top_k_words)))
                num_written_lines += 1
                convert_pred.append([preds, top_k_words_info, top_k_sents_info])

        return convert_pred


if __name__ == '__main__':
    config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/xdc_config.json', 'r'))
    model = BertXDC(config, 'ko')

    context = "전주맛집 중 빠질 수 없는 전주 먹거리는 바로 막걸리다. "\
              "전주에서 막걸리 하면 상다리가 휠만큼 많은 안주들을 맛볼 수 있으며 푸짐한 인심으로도 유명하다. "\
              "그중 맛은 물론 합리적인 가격과 분위기, 볼거리가 많은 \"한옥전주막걸리\"는 "\
              "넉넉한 인심과 친절한 서비스를 자랑한다. "\
              "가족과의 외식, 회식장소로도 많은 관심을 받고 있는 \"한옥전주막걸리\"는 "\
              "전체 인원 200명 수용 가능한 내부 크기와 야외 테라스 및 최신 트렌드에 걸맞는 젊은층을 "\
              "겨냥한 인테리어를 자랑하고, 10가지 이상의 기본 안주는 물론, 메인 안주에도 노력을 쏟아 "\
              "방문객들의 재방문이 줄을 잇고 있다. 매일 시간마다 다른 구성으로 즐길 수 있는 색다른 라이브 공연으로 "\
              "눈과 귀를 만족시켜주는 장점을 갖춰 다른 막걸리집과 차별함을 두고 있으며, 다양한 볼거리가 많은 "\
              "전주한옥마을에서 5분 정도의 가까운 거리에 위치해있어 전주를 찾는 관광객들에게도 많은 관심을 받고 있다. "\
              "젊은층에게도 인기 있는 \"한옥전주막걸리\"는 언제나 건강한 재료를 사용하고 남녀노소 즐길 수 있는 "\
              "막걸리를 널리 알리기 위해 전 직원이 노력하고 있다고 관계자는 밝혔다. 전라북도 블로거들이 적극 추천하는 "\
              "인증 먹거리 \"한옥전주막걸리\"는 전주 우아동에 위치하고 있으며 오후 4시부터 오전 2시까지 영업하고 있다. "\
              "공연 및 단체 예약문의는 063-241-4466으로 가능하다."

    out = model.infer(context)
    print(out)
