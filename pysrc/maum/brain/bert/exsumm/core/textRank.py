import re
import nltk
import numpy as np
import konlpy.tag as tag
from collections import OrderedDict


def convert_text(text):
   out = re.sub('[?]', '?. ',
                re.sub('다\.', '다.. ',
                       re.sub('\."', '"',
                              re.sub('[!]', '!. ',
                                     re.sub('[.?]+', '.', re.sub('[ ]+', ' ', re.sub('[.]+', '.', text)))))))
   return out


class TextRank:
    def __init__(self, steps=10, d=0.85, min_diff=1e-5):
        self.steps = steps  # iteration steps
        self.d = d  # damping coefficient, usually is .85
        self.min_diff = min_diff  # convergence threshold

    def sentence_segment(self, doc, candidate_pos=None, lower=False):
        """Store those words only in cadidate_pos"""
        nlp = tag.Mecab()
        sentences = []
        for sent in [i.strip() for i in convert_text(doc).split('. ')][:-1]:
        # for sent in nltk.sent_tokenize(doc):
            selected_words = []
            for token in nlp.pos(sent):
                if len(token[0]) < 2:
                    continue
                if candidate_pos is not None:
                    if token[1] in candidate_pos:
                        if lower is True:
                            selected_words.append(token[0].lower())
                        else:
                            selected_words.append(token[0])
                else:
                    if lower is True:
                        selected_words.append(token[0].lower())
                    else:
                        selected_words.append(token[0])
            sentences.append(selected_words)
        return sentences

    def get_vocab(self, sentences):
        """Get all tokens"""
        vocab = OrderedDict()
        i = 0
        for sentence in sentences:
            for word in sentence:
                if word not in vocab:
                    vocab[word] = i
                    i += 1
        return vocab

    def get_token_pairs(self, window_size, sentences):
        """Build token_pairs from windows in sentences"""
        token_pairs = list()
        for sentence in sentences:
            for i, word in enumerate(sentence):
                for j in range(i + 1, i + window_size):
                    if j >= len(sentence):
                        break
                    pair = (word, sentence[j])
                    if pair not in token_pairs:
                        token_pairs.append(pair)
        return token_pairs

    def get_word_matrix(self, vocab, token_pairs):
        vocab_size = len(vocab)
        g = np.zeros((vocab_size, vocab_size), dtype='float')
        for word1, word2 in token_pairs:
            i, j = vocab[word1], vocab[word2]
            g[i][j] = 1
        g = g + g.T - np.diag(g.diagonal())

        norm = np.sum(g, axis=0)
        g_norm = np.divide(g, norm, where=norm != 0)  # this is ignore the 0 element in norm
        return g_norm

    def get_words_rank(self, text, num=10, window_size=4, lower=False, stopwords=list()):
        candidate_pos = ['NNP', 'NNG']
        # Filter sentences
        sentences = self.sentence_segment(text, candidate_pos, lower)  # list of list of words

        # Build vocabulary
        vocab = self.get_vocab(sentences)

        # Get token_pairs from windows
        token_pairs = self.get_token_pairs(window_size, sentences)

        # Get normalized matrix
        g = self.get_word_matrix(vocab, token_pairs)

        # Initionlization for weight(pagerank value)
        pr = np.array([1] * len(vocab))

        # Iteration
        previous_pr = 0
        for epoch in range(self.steps):
            pr = (1 - self.d) + self.d * np.dot(g, pr)
            if abs(previous_pr - sum(pr)) < self.min_diff:
                break
            else:
                previous_pr = sum(pr)

        out = []
        for i in np.argsort(-pr)[:num]:
            rank_sent = list(vocab)[i]
            rank_score = pr[i]
            print('%s - %.6f' % (rank_sent, rank_score))
            out.append([rank_sent, rank_score])
        return out

    def cosim(self, mat):
        g = np.dot(mat, mat.T)
        g = g + - np.diag(g.diagonal())
        norm = np.array([[np.linalg.norm(i)] for i in mat])
        norm = np.dot(norm, norm.T)
        norm = norm - np.diag(norm.diagonal() - 1e-6)
        return g / norm

    def get_sent_matrix(self, vocab, sentences, cosim):
        """Get normalized matrix"""
        # Build matrix
        mat = np.zeros((len(sentences), len(vocab)), dtype='float')
        for i, sent in enumerate(sentences):
            for w in sent:
                j = vocab[w]
                mat[i][j] = 1
        # tf_mat = (mat.T * (1 / np.sum(mat, 1))).T
        # idf_mat = np.log(len(sentences) / (1 + np.sum(mat, 0))) * mat
        # tfidf_mat = tf_mat * idf_mat
        tfidf_mat = np.log(len(sentences) / (1 + np.sum(mat, 0))) * mat
        if not cosim:
            g = np.dot(tfidf_mat, tfidf_mat.T)
            g = g + g.T - np.diag(g.diagonal())
            # Normalize matrix by column
            norm = np.sum(g, axis=0)
            g_norm = np.divide(g, norm, where=norm != 0)  # this is ignore the 0 element in norm
        else:
            g_norm = self.cosim(tfidf_mat)
        return g_norm

    def get_sents_rank(self, text, num=3, has_title=True, cosim=False, lower=False, stopwords=list()):
        candidate_pos = ['NNP', 'NNG']#, 'NNB', 'NR', 'NP', 'VV', 'VA', 'VX', 'VCP', 'VCN']
        # candidate_pos = ['IC', 'JKS', 'JKC', 'JKG', 'JKO', 'JKB',
        #                  'JKV', 'JKQ', 'JX', 'JC', 'SF', 'SE', 'SS', 'SP', 'SO', 'SW']

        # Filter sentences
        sentences = self.sentence_segment(text, candidate_pos=None, lower=lower)  # list of list of words

        # Build vocabulary
        vocab = self.get_vocab(sentences)

        # Get normalized matrix
        g = self.get_sent_matrix(vocab, sentences, cosim)
        if has_title:
            t = g[0][1:]
            g = t * g[1:, 1:]
            sentences = sentences[1:]

        # Initionlization for weight(pagerank value)
        pr = np.array([1] * len(sentences))

        # Iteration
        previous_pr = 0
        for epoch in range(self.steps):
            pr = (1 - self.d) + self.d * np.dot(g, pr)
            if abs(previous_pr - sum(pr)) < self.min_diff:
                break
            else:
                previous_pr = sum(pr)

        out = []
        for i in np.argsort(-pr)[:num]:
            if has_title:
                rank_sent = [i.strip() for i in convert_text(text).split('. ')][1:-1][i]
                # rank_sent = nltk.sent_tokenize(text)[1:][i]
            else:
                rank_sent = [i.strip() for i in convert_text(text).split('. ')][:-1][i]
                # rank_sent = nltk.sent_tokenize(text)[i]
            rank_score = pr[i]
            # print('%s - %.6f' % (rank_sent, rank_score))
            out.append([rank_sent, rank_score])
        return out, np.argsort(-t)[0]


if __name__ == '__main__':
    title = '''백자대호 31억원 낙찰… 국내 경매 도자기 최고가 경신'''
    body = '''
    조선시대 달항아리 백자대호가 서울옥션 경매에서 31억원에 낙찰되며 국내 미술 경매 시장에서 거래된 도자기 중 최고가를 경신했다.
    
    서울옥션은 26일 오후 강남센터에서 열린 152회 경매에서 높이 45.5㎝, 너비 41㎝의 백자대호가 23억원에 경매를 시작해 경합 끝에 31억원에 낙찰됐다고 27일 밝혔다. 눈처럼 흰 바탕색과 둥근 형태로, 비례가 안정적이고 단정하다.
    이날 경매에선 백자를 소재로 한 다양한 근ㆍ현대 회화 작품도 모두 팔려나갔다. 백자를 중심으로 자연을 그려낸 김환기의 ‘항아리’는 9억원에 낙찰됐고, 백자 항아리에 꽃이 꽂힌 장면을 담은 도상봉의 ‘꽃’은 6,500만원에 낙찰됐다. 서울옥션 관계자는 “한국 고유의 순백의 아름다움 에 대한 컬렉터의 관심을 엿볼 수 있었다”고 평가했다. 
    
    이번 경매는 미술시장에서 꾸준한 인기를 얻고 있는 한국 고미술 작품의 경합이 돋보였다. 겸재 정선, 호생관 최북, 추사 김정희 등의 작품이 좋은 반응을 얻었다. 겸재 정선의 작품은 총 3점이 출품되었는데, ‘메추라기’와 ‘제비’, ‘수쇄탕주인’은 모두 시작가의 2배 이상을 웃도는 가격에 낙찰됐다. 숙종 1681년에 제작한 보물 제1239호인 ‘감로탱화(甘露幀畵)’는 11억원에 시작해 12억5,000만원에 새 주인을 찾았다.
    '''

    body = re.sub(r'[\n]+', '\n', body)

    text = title+'.'+body

    # input_test = ['중국 베이징 국가대극원에서 프로코피예프 모음곡과 브람스 교향곡 4번 연주해 서울시립교향악단이 아시아 최대 규모를 자랑하는 중국 베이징 국가대극원의 초청으로 중국 관객과 만난다', '서울시향은 오는 19일 국가대극원 콘서트홀에서 정명훈 예술감독의 지휘로 프로코피예프의 ‘로미오와 줄리엣’ 모음곡과 브람스 교향곡 4번을 연주한다', '그동안 중국에서 한중 교류 차원의 초청 공연을 한 적은 있지만 서울시향의 이름과 음악을 내걸고 전석 유료 판매의 공연을 하는 것은 이번이 처음이다', '이번에 서울시향을 초청한 국가대극원은 베이징의 문화예술을 상징하는 아시아 최대 규모의 공연장이다', '주빈 메타, 크리스토프 에센바흐 등 세계적 지휘자들이나 피아니스트 랑랑 등 연주자들이 이곳을 다녀갔다', '정 예술감독도 지난달 20일부터 나흘간 테너 플라시도 도밍고와 국가대극원 오케스트라와 함께 베르디의 오페라 ‘시몬 보카네그라’를 지휘했다', '서울시향은 이번에 2,017석 규모의 콘서트홀에서 연주한다\n']
    # input_test = '. '.join(input_test)

    tr = TextRank(10)
    tr.get_words_rank(text)
    tr.get_sents_rank(text, cosim=True, has_title=True)
