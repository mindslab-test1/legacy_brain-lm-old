import tensorflow as tf
import tensorflow.contrib.layers as layers
from tensorflow.contrib.layers.python.layers import batch_norm
try:
    from tensorflow.contrib.rnn import LSTMStateTuple
except ImportError:
    LSTMStateTuple = tf.nn.rnn_cell.LSTMStateTuple
import json


def bidirectional_rnn(cell_fw, cell_bw, inputs_embedded, input_lengths,
                      scope=None):
    """Bidirecional RNN with concatenated outputs and states"""
    with tf.variable_scope(scope or "birnn") as scope:
        ((fw_outputs,
          bw_outputs),
         (fw_state,
          bw_state)) = (
            tf.nn.bidirectional_dynamic_rnn(cell_fw=cell_fw,
                                            cell_bw=cell_bw,
                                            inputs=inputs_embedded,
                                            sequence_length=input_lengths,
                                            dtype=tf.float32,
                                            swap_memory=True,
                                            scope=scope))
        outputs = tf.concat((fw_outputs, bw_outputs), 2)

        def concatenate_state(fw_state, bw_state):
            if isinstance(fw_state, LSTMStateTuple):
                state_c = tf.concat(
                    (fw_state.c, bw_state.c), 1, name='bidirectional_concat_c')
                state_h = tf.concat(
                    (fw_state.h, bw_state.h), 1, name='bidirectional_concat_h')
                state = LSTMStateTuple(c=state_c, h=state_h)
                return state
            elif isinstance(fw_state, tf.Tensor):
                state = tf.concat((fw_state, bw_state), 1,
                                  name='bidirectional_concat')
                return state
            elif (isinstance(fw_state, tuple) and
                    isinstance(bw_state, tuple) and
                    len(fw_state) == len(bw_state)):
                # multilayer
                state = tuple(concatenate_state(fw, bw)
                              for fw, bw in zip(fw_state, bw_state))
                return state

            else:
                raise ValueError(
                    'unknown state type: {}'.format((fw_state, bw_state)))


        state = concatenate_state(fw_state, bw_state)
        return outputs, state


def task_specific_attention(inputs, output_size, projection_output,
                            initializer=layers.xavier_initializer(),
                            activation_fn=tf.tanh, temp = 1, scope=None):
    """
    Performs task-specific attention reduction, using learned
    attention context vector (constant within task of interest).

    Args:
        inputs: Tensor of shape [batch_size, units, input_size]
            `input_size` must be static (known)
            `units` axis will be attended over (reduced from output)
            `batch_size` will be preserved
        output_size: Size of output's inner (feature) dimension

    Returns:
        outputs: Tensor of shape [batch_size, output_dim].
    """
    assert len(inputs.get_shape()) == 3 and inputs.get_shape()[-1].value is not None

    with tf.variable_scope(scope or 'attention') as scope:
        attention_context_vector = tf.get_variable(name='attention_context_vector',
                                                   shape=[output_size],
                                                   initializer=initializer,
                                                   dtype=tf.float32)
        input_projection = layers.fully_connected(inputs, output_size,
                                                  activation_fn=activation_fn,
                                                  scope=scope)

        vector_attn = tf.reduce_sum(tf.multiply(input_projection, attention_context_vector), axis=2, keepdims=True)
        attention_weights = tf.nn.softmax(vector_attn, dim=1)
        weighted_projection = tf.multiply(input_projection, attention_weights)

        if projection_output == False:
            outputs = tf.reduce_sum(weighted_projection, axis=1)
            return outputs, attention_weights
        else:
            return weighted_projection, attention_weights


def word_sent_padding(batch_input_feature, thr_sentseq, thr_wordseq, feature_dim):

    padding_feature_stack = []
    for input_feature in batch_input_feature:

        temp_sentence_stack = []
        for sentence_ in input_feature:
            if len(sentence_) > thr_wordseq:
                sentence_temp = sentence_[:thr_wordseq]
            elif len(sentence_) < thr_wordseq:
                sentence_temp = sentence_ + ([[0] * feature_dim] * (thr_wordseq - len(sentence_)))
            else:
                sentence_temp = sentence_

            temp_sentence_stack.append(sentence_temp)

        if len(temp_sentence_stack) > thr_sentseq:
            padding_feature_temp = temp_sentence_stack[:thr_sentseq]
        elif len(temp_sentence_stack) < thr_sentseq:
            padding_feature_temp = temp_sentence_stack + [[[0] * feature_dim] * thr_wordseq] * (thr_sentseq - len(temp_sentence_stack))
        else:
            padding_feature_temp = temp_sentence_stack
        padding_feature_stack.append(padding_feature_temp)

    return padding_feature_stack


def batchnorm_layer(inputT, is_training, name=None):
    # Note: is_training is tf.placeholder(tf.bool) type
    return tf.cond(is_training,
                   lambda: batch_norm(inputT, is_training=is_training,
                                      center=True, scale=True, activation_fn=None,
                                      decay=0.9, scope=name,
                                      updates_collections=None,
                                      zero_debias_moving_mean=True),

                   lambda: batch_norm(inputT, is_training=is_training,
                                      center=True,
                                      scale=True,
                                      activation_fn=None,
                                      decay=0.9, scope=name,
                                      reuse=True,
                                      updates_collections=None,
                                      zero_debias_moving_mean=True))


def global_pooling(inputs, scope_name, pooling_type='concat'):
   with tf.variable_scope(scope_name + '_Global_average_pooling'):
       gap = tf.layers.average_pooling1d(inputs,
                                         pool_size=int(inputs.shape[1]),
                                         strides=1, padding='valid')
       gap = tf.reduce_mean(gap, axis=1)

   with tf.variable_scope(scope_name + '_Global_max_pooling'):
       gmp = tf.layers.max_pooling1d(inputs,
                                     pool_size=int(inputs.shape[1]),
                                     strides=1, padding='valid')
       gmp = tf.reduce_mean(gmp, axis=1)

   with tf.variable_scope(scope_name + '_pooling_concat_layer'):
       pooling_concat_layer = tf.concat([gap, gmp], axis=-1)

   if pooling_type == 'average':
       return gap
   elif pooling_type == 'max':
       return gmp
   elif pooling_type == 'concat':
       return pooling_concat_layer


def conv_1d_with_batch(inputs, num_featrue, kernel, is_training, trainable, name):
   with tf.variable_scope(name + '_conv1d'):
       conv_layer = tf.layers.conv1d(inputs,
                                     filters=num_featrue,
                                     kernel_size=kernel,
                                     strides=1,
                                     padding='SAME',
                                     activation=None,
                                     kernel_initializer=tf.contrib.layers.variance_scaling_initializer(),
                                     use_bias=False,
                                     name=name,
                                     trainable=trainable)
       conv_batch_norm = batchnorm_layer(conv_layer, is_training, name + '_batchnorm')
   return tf.nn.relu(conv_batch_norm)


def load_batch(json_path, length_list, size):
    global opened_json_train
    global epoch_train

    doc_list = []
    line_idx = []
    for iter_ in range(size):

        try:
            doc_dict_ = opened_json_train.__next__()
            epoch_switch = 0

        except StopIteration:
            opened_json_train = open(json_path)
            # doc_dict_ = opened_json_train.__next__()
            epoch_train += 1
            epoch_switch = 1
            break
        except NameError:
            opened_json_train = open(json_path)
            doc_dict_ = opened_json_train.__next__()
            epoch_train = 0
            epoch_switch = 0

        doc_dict = json.loads(doc_dict_)

        this_doc_idx = doc_dict['linex_index']
        this_length_list = length_list[this_doc_idx]

        sentence_stack = []
        word_stack_temp = []
        word_seq_idx = 0

        for word_idx in range(len(doc_dict['features'])):
            if word_seq_idx == len(this_length_list):
                break
            if word_idx == 0:
                continue
            word_stack_temp.append(doc_dict['features'][word_idx]['layers'][0]['values'])
            if (len(word_stack_temp) == this_length_list[word_seq_idx]) or (word_idx + 1 == len(doc_dict['features'])):
                sentence_stack.append(word_stack_temp)
                word_seq_idx += 1
                word_stack_temp = []
                continue

        line_idx.append(this_doc_idx)
        doc_list.append(sentence_stack)
    if epoch_switch == 1:
        return epoch_train - 1, line_idx, doc_list
    else:
        return epoch_train, line_idx, doc_list


def load_batch_test(json_path, length_list, size):
    global opened_json_test
    global epoch_test

    doc_list = []
    line_idx = []
    for iter_ in range(size):

        try:
            doc_dict_ = opened_json_test.__next__()
            epoch_switch = 0

        except StopIteration:
            opened_json_test = open(json_path)
            # doc_dict_ = opened_json_train.__next__()
            epoch_test += 1
            epoch_switch = 1
            break

        except NameError:
            opened_json_test = open(json_path)
            doc_dict_ = opened_json_test.__next__()
            epoch_test = 0
            epoch_switch = 0

        doc_dict = json.loads(doc_dict_)

        this_doc_idx = doc_dict['linex_index']
        this_length_list = length_list[this_doc_idx]

        sentence_stack = []
        word_stack_temp = []
        word_seq_idx = 0

        for word_idx in range(len(doc_dict['features'])):
            if word_seq_idx == len(this_length_list):
                break
            if word_idx == 0:
                continue
            word_stack_temp.append(doc_dict['features'][word_idx]['layers'][0]['values'])
            if (len(word_stack_temp) == this_length_list[word_seq_idx]) or (word_idx + 1 == len(doc_dict['features'])):
                sentence_stack.append(word_stack_temp)
                word_seq_idx += 1
                word_stack_temp = []
                continue

        line_idx.append(this_doc_idx)
        doc_list.append(sentence_stack)

    if epoch_switch == 1:
        return epoch_test - 1, line_idx, doc_list
    else:
        return epoch_test, line_idx, doc_list
