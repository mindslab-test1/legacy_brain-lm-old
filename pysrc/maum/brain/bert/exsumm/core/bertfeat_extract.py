import tensorflow as tf
import collections
import json
import os
import sys

import modeling
import tokenization
import bertfeat_core


class BertFeat:
    def __init__(self, config, lang, max_seq_length, init_checkpoint=None, device=None, is_training=False):
        if device is None:
            device = 0
        os.environ['CUDA_VISIBLE_DEVICES'] = str(device)

        if type(config) == str:
            config = json.load(open(config))[lang]
        else:
            config = config[lang]

        self.config = config
        self.dir = config['dir']
        self.is_training = is_training
        self.bert_config = modeling.BertConfig.from_json_file(config['bert_config'])

        print('loaded ckpt graph')
        if max_seq_length > 512:
            raise ValueError('sequence length less than 512')
        self.max_seq_length = max_seq_length
        self.input_ids = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='input_ids')
        self.input_mask = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='input_mask')
        self.segment_ids = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='segment_ids')

        model = modeling.BertModel(
            config=self.bert_config,
            is_training=is_training,
            input_ids=self.input_ids,
            input_mask=self.input_mask,
            token_type_ids=self.segment_ids,
            use_one_hot_embeddings=False,
            vocab_size=None)

        self.pooled_out = model.get_pooled_output()
        self.seq_out = model.get_sequence_output()

        tvars = tf.trainable_variables()
        initialized_variable_names = {}
        init_checkpoint = self.dir + config['init_checkpoint']
        if init_checkpoint is None:
            init_checkpoint = self.dir+config['init_checkpoint'] + \
                              [i for i in os.listdir(self.dir + config['init_checkpoint']) if '.ckpt' in i][-1][:-5]
        if init_checkpoint:
            (assignment_map, initialized_variable_names) = \
                modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
            tf.train.init_from_checkpoint(init_checkpoint, assignment_map)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        self.sess.run(tf.global_variables_initializer())
        self.sess.run(self.pooled_out, feed_dict={self.input_ids: [[0 for _ in range(self.input_ids.shape[-1])]],
                                                  self.input_mask: [[0 for _ in range(self.input_mask.shape[-1])]],
                                                  self.segment_ids: [[0 for _ in range(self.segment_ids.shape[-1])]]})

        # tf.gfile.MakeDirs(self.dir + config['output_dir'])
        self.tokenizer = tokenization.FullTokenizer(vocab_file=self.dir + config['vocab_file'],
                                                    do_lower_case=config['do_lower_case'])
        self.processor = bertfeat_core.XdcProcessor()

    def get_feat(self, input_doc, seq_out=True):
        examples = self.processor._create_examples([[input_doc]], set_type='perd', labels=None)

        eval_features = []
        for (ex_index, example) in enumerate(examples):
            feature = bertfeat_core.convert_single_example(
                ex_index, example, self.max_seq_length, self.tokenizer, True, 30)

            features = collections.OrderedDict()
            features["input_ids"] = feature.input_ids
            features["input_mask"] = feature.input_mask
            features["segment_ids"] = feature.segment_ids
            eval_features.append(features)

        feed_dict = {self.input_ids: [features['input_ids'] for features in eval_features],
                     self.input_mask: [features['input_mask'] for features in eval_features],
                     self.segment_ids: [features['segment_ids'] for features in eval_features]}

        if seq_out:
            out = self.sess.run(self.seq_out, feed_dict=feed_dict)
        else:
            out = self.sess.run(self.pooled_out, feed_dict=feed_dict)

        return out


if __name__ == '__main__':
    input_doc = '''조선시대 달항아리 백자대호가 서울옥션 경매에서 31억원에 낙찰되며 국내 미술 경매 시장에서 거래된 도자기 중 최고가를 경신했다.
    서울옥션은 26일 오후 강남센터에서 열린 152회 경매에서 높이 45.5㎝, 너비 41㎝의 백자대호가 23억원에 경매를 시작해 경합 끝에 31억원에 낙찰됐다고 27일 밝혔다. 눈처럼 흰 바탕색과 둥근 형태로, 비례가 안정적이고 단정하다.
    이날 경매에선 백자를 소재로 한 다양한 근ㆍ현대 회화 작품도 모두 팔려나갔다. 백자를 중심으로 자연을 그려낸 김환기의 ‘항아리’는 9억원에 낙찰됐고, 백자 항아리에 꽃이 꽂힌 장면을 담은 도상봉의 ‘꽃’은 6,500만원에 낙찰됐다. 서울옥션 관계자는 “한국 고유의 순백의 아름다움 에 대한 컬렉터의 관심을 엿볼 수 있었다”고 평가했다. 
    이번 경매는 미술시장에서 꾸준한 인기를 얻고 있는 한국 고미술 작품의 경합이 돋보였다. 겸재 정선, 호생관 최북, 추사 김정희 등의 작품이 좋은 반응을 얻었다. 겸재 정선의 작품은 총 3점이 출품되었는데, ‘메추라기’와 ‘제비’, ‘수쇄탕주인’은 모두 시작가의 2배 이상을 웃도는 가격에 낙찰됐다. 숙종 1681년에 제작한 보물 제1239호인 ‘감로탱화(甘露幀畵)’는 11억원에 시작해 12억5,000만원에 새 주인을 찾았다.'''

    ff = BertFeat(config='./bertfeat_config.json', lang='ko', device=0, max_seq_length=128)

    print(ff.get_feat(input_doc, seq_out=True).shape)
    print(ff.get_feat(input_doc, seq_out=False).shape)
