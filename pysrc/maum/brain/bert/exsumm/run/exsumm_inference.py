import numpy as np
import re

from ..core.textRank import TextRank
from ..core.model_components import *
from ..core.bertfeat_extract import *


def convert_text(text):
   '''
   convert text for sentence split
   split token: '.. '
   '''
   out = re.sub('[\n]+', '',
                re.sub('[?]', '?.. ',
                       re.sub('다\.', '다... ',
                              re.sub('\."', '"',
                                     re.sub('[!]', '!.. ',
                                            re.sub('[.?]+', '.',
                                                   re.sub('[ ]+', ' ',
                                                          re.sub('[.]+', '.', text))))))))
   return out


def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]


def load_batch_test(input_feature, sent_length, total_seq_len):
    sentence_stack = []
    word_stack_temp = []
    word_seq_idx = 0
    if total_seq_len > 512:
        total_seq_len = 512
    for word_idx in range(total_seq_len):
        if word_seq_idx == len(sent_length):
            break
        if word_idx == 0:
            continue
        word_stack_temp.append(input_feature[word_idx])
        if (len(word_stack_temp) == sent_length[word_seq_idx]) or (word_idx + 1 == total_seq_len):
            sentence_stack.append(word_stack_temp)
            word_seq_idx += 1
            word_stack_temp = []
            continue

    return sentence_stack


def orderedset(list_):
    my_set = set()
    res = []
    for e in list_:
        if e not in my_set:
            res.append(e)
            my_set.add(e)

    return res


class BertExSumm:
    def __init__(self,
                 config='/home/minds/.virtualenvs/venv_bert/ex_bert/exsumm_config.json',
                 model="/home/minds/.virtualenvs/venv_bert/xdc_feature_extract/cnn_model_attention/model_epoch14.ckpt",
                 lang='ko', device=0):

        os.environ['CUDA_VISIBLE_DEVICES'] = str(device)
        config = json.load(open(config))
        sentence_rep_size = 256
        filter_sizes_sentence = [3, 4, 5]
        num_filters_sentence = 128
        doc_rep_size = 64
        self.batch_size = 1
        self.thr_wordseq = 45
        self.thr_sentseq = 30
        self.feature_dim = 768
        self.top_k_sent = 3
        self.top_k_word = 4

        self.tokenizer = tokenization.FullTokenizer(vocab_file=config[lang]['dir']+config[lang]['vocab_file'],
                                                    do_lower_case=False)

        # [document x sentence x word]
        self.inputs = tf.placeholder(shape=(None, self.thr_sentseq, self.thr_wordseq, self.feature_dim),
                                     dtype=tf.float32,
                                     name='input_features')

        self.batch_norm_train = tf.placeholder(tf.bool, name='phase')

        word_level_inputs = tf.reshape(self.inputs, shape=[-1, self.thr_wordseq, self.feature_dim])

        word_att_stack = []
        att_out_word, att_weight_word = task_specific_attention(
            word_level_inputs,
            sentence_rep_size,
            projection_output=False,
            scope='word_attention')
        word_att_stack.append(att_weight_word)

        self.word_att_stack = word_att_stack

        sentence_inputs = tf.reshape(
            att_out_word, [-1, self.thr_sentseq, att_out_word.shape[-1]])

        ####### sentence
        outputs_sent_att_weight = []
        for i, filter_size in enumerate(filter_sizes_sentence):
            with tf.name_scope("sentence-conv-maxpool-%s" % filter_size):
                bn_conv = conv_1d_with_batch(sentence_inputs, num_filters_sentence, filter_size,
                                             self.batch_norm_train, True, 'bn-%s' % filter_size)

            # Attention
            Attention_out_sent, att_weight_sent = task_specific_attention(
                bn_conv,
                doc_rep_size,
                projection_output=True,
                scope='sentence_attention-%s' % filter_size)

            outputs_sent_att_weight.append(tf.reshape(att_weight_sent, [-1, att_weight_sent.shape[1]]))

        self.outputs_sent_att_weight = outputs_sent_att_weight

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        saver = tf.train.Saver()
        saver.restore(self.sess, model)

        self.bert_extractor = BertFeat(config=config, lang=lang, max_seq_length=512)

        self.tr = TextRank()

    def infer(self, text, title):
        bert_output = self.bert_extractor.get_feat(text, seq_out=True)
        _, tr_out = self.tr.get_sents_rank(title+'.'+text, cosim=True, has_title=True)

        sent_split = [i.strip() for i in convert_text(text).split('.. ')]
        sent_split = sent_split[:-1]

        temp_sentence = []
        temp_sentence_len = []
        temp_token_sentence = []
        for xx_sent in sent_split:
            temp_token = self.tokenizer.tokenize(xx_sent)
            xx_sent_len = len(temp_token)
            temp_token_sentence.append(temp_token)

            temp_sentence.append(xx_sent)
            temp_sentence_len.append(xx_sent_len)

        total_seq = np.sum(temp_sentence_len) + 2  # include start, end

        no_padd_inp = load_batch_test(bert_output.squeeze(), temp_sentence_len, total_seq)
        padding_input_test = np.array(word_sent_padding([no_padd_inp], self.thr_sentseq, self.thr_wordseq, self.feature_dim))

        feed_dict_ = {self.inputs: padding_input_test,
                      self.batch_norm_train: False}

        s_at, w_at = self.sess.run([self.outputs_sent_att_weight, self.word_att_stack], feed_dict_)
        attention_ = np.mean(np.array(s_at).squeeze(), axis=0)
        attention_sort = orderedset([int(tr_out)] + np.flip(attention_.argsort(), axis=0).tolist())
        # attention_sort = np.flip(attention_.argsort(), axis=0)
        attention_w = np.array(w_at).squeeze()

        top_sent_list = []
        top_word_list = []
        for sent_num, sent_ind in enumerate(attention_sort[:self.top_k_sent]):
            try:
                top_sent_list.append(temp_sentence[sent_ind])
                attention_sort_w = np.flip(attention_w[sent_ind].argsort(), axis=0)
            except:
                continue
            temp_lis = []
            for word_id in attention_sort_w:

                if len(temp_lis) > 4-sent_num:
                    break

                try:
                    if len(temp_token_sentence[sent_ind][word_id].replace('##', '')) < 2:
                        continue
                    else:
                        selected_token = temp_token_sentence[sent_ind][word_id]
                        if '##' in selected_token:
                            selected_token = temp_token_sentence[sent_ind][word_id-1:word_id+1]
                            selected_token = ''.join(selected_token).replace('##', '')
                        temp_lis.append(selected_token)
                except:
                    continue
            top_word_list.extend(temp_lis)

        return top_sent_list, orderedset(top_word_list)


if __name__ == '__main__':
    model = BertExSumm()

    text = '''
    송송커플(배우 송중기·송혜교)의 이혼 소식에 스튜디오드래곤과 블러썸엠앤씨 등 상장사 주가도 '화들짝' 놀란 모습이다. 
    블러썸엠앤씨는 배우 송중기의 소속사(블러썸엔터테인먼트) 대표가 임원으로 재직 중인 곳이고, 스튜디오드래곤은 송중기가 주연인 아스달연대기를 제작한 곳이다. 
    27일 이날 오전 11시 23분 현재 코스닥 시장에서 블러썸엠앤씨는 전날보다 100원(0.4%) 하락한 2만4900원에 거래되고 있다. 장중 2만4300원까지 하락하기도 했다. 
    주가가 하락하는 것은 블러썸엠앤씨가 잇달아 영상, 영화, 방송프로그램 제작 업체 등의 주식을 양수한데 따른 것으로 풀이된다. 
    블러썸엠앤씨는 전날 방송프로그램 제작 및 판매업을 영위하는 주식회사 아이비스포츠의 주식 12만주를 60억원에 인수했다. 
    앞서 11일에는 영상콘텐츠를 제작하는 블러썸스토리 주식 100만주를 100억원에 인수했고 영화 및 영상물제작, 영화수입 배급업 등을 영위하는 블러썸픽쳐스 주식 1만1111주도 150억원에 사들였다. 
    특히 블러썸엠앤씨의 지영주 대표이사와 주방옥 이사는 송중기 씨의 소속사인 블러썸엔터테인먼트의 공동대표다. 
    블러썸엔터테인먼트도 블러썸앰엔씨에 합류할 가능성이 높다는 의미다. 
    다만 블러썸엠앤씨와 블러썸엔터테인먼트는 실제 지분 거래나 사업 제휴 등의 움직임은 보이지 않고 있다.
    스튜디오드래곤도 관심사다. 스튜디오드래곤은 '아스달연대기'를 앞세워 흥행을 노리고 있다. 아스달연대기에는 송중기가 출연한다. 
    이날 오전 11시 23분 기준 코스닥 시장에서 스튜디오드래곤은 전날보다 900원(1.29%) 내린 6만8800원에 거래되고 있다. 
    주가의 부진은 아스달연대기가 기대에 미치지 못했다는 평가가 우세하기 때문이라는 설명이다. 송중기의 이혼과는 별개라는 의미다. 
    이기훈 하나금융투자 연구원은 "최근 주가 하락은 아스달연대기에 대한 실망감에서 비롯됐지만 하반기에는 작품들의 제작비 안정화 등으로 수익성이 점진적으로 상향될 것"이라고 봤다. 
    엔터업계 관계자는 "송중기의 이혼은 사생활이기 때문에 아스달연대기에 직접적인 영향을 주긴 어려울 것"이라며 "심지어 송중기의 출연분은 이미 촬영도 마친 상태"라고 귀띔했다.
    '''
    title = '송송커플 이혼하자 블러썸엠앤씨·스튜디오드래곤 주가 \'화들짝\''

    print(model.infer(text, title))
