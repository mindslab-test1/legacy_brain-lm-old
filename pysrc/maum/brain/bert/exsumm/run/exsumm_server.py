from concurrent import futures

import logging
import time
import grpc
import argparse
import sys, os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.exsumm.exsumm_inference import BertExSumm as BEX
from maum.brain.bert.exsumm import bex_pb2
from maum.brain.bert.exsumm.bex_pb2_grpc import BertExSummServicer
from maum.brain.bert.exsumm.bex_pb2_grpc import add_BertExSummServicer_to_server


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class BertExSumm(BertExSummServicer):
    def __init__(self, config, model, lang, device):
        self.model = BEX(config=config, model=model, lang=lang, device=device)

    def ExSummary(self, msg, context):

        exout = self.model.infer(msg.body, msg.title)

        exwords = []
        for w in exout[1]:
            exword = bex_pb2.ExWord()
            exword.word = w
            exwords.append(exword)
        exsents = []
        for s in exout[0]:
            exsent = bex_pb2.ExSent()
            exsent.sent = s
            exsents.append(exsent)

        output = bex_pb2.Outputs()
        output.exwords.extend(exwords)
        output.exsents.extend(exsents)

        return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='bertexsumm runner executor')
    print(parser.description)
    parser.add_argument('-c', '--config',
                        type=str,
                        default='/home/minds/.virtualenvs/venv_bert/ex_bert/exsumm_config.json')
    parser.add_argument('-m', '--model',
                        type=str,
                        default="/home/minds/.virtualenvs/venv_bert/xdc_feature_extract/cnn_model_attention/model_epoch14.ckpt")
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='ko')
    parser.add_argument('-d', '--device',
                        type=int,
                        default=0)
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=35004)
    args = parser.parse_args()

    bertexsumm = BertExSumm(args.config, args.model, args.lang, args.device)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_BertExSummServicer_to_server(bertexsumm, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('bertxdc starting at 0.0.0.0:%d' % args.port)
    print('bertxdc starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
