import os
import sys
import json
import random
import argparse
import collections
import _pickle as pk
import tensorflow as tf

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.mrc.core import modeling
from maum.brain.bert.mrc.core import tokenization
from maum.brain.bert.mrc.core import squad_core
from maum.brain.bert.mrc.train import evaluate_v1


class BertSquad:
    def __init__(self, config, num_gpu):
        tf.logging.set_verbosity(tf.logging.INFO)
        self.config = config
        self.dir = os.environ['MAUM_ROOT']
        self.num_gpu = num_gpu

    def runner(self, train_batch_size, max_seq_length, num_train_epochs, train, en, model_type, num_freeze,
               train_file=None, predict_file=None):
        if not en:
            self.config = self.config['ko']
        else:
            self.config = self.config['en']

        bert_config = modeling.BertConfig.from_json_file(self.config['bert_config'])

        tokenizer = tokenization.FullTokenizer(
            vocab_file=self.dir+self.config['vocab_file'],
            do_lower_case=en)

        train_examples = None
        num_train_steps = None
        num_warmup_steps = None
        if train:
            if train_file is None:
                raise ValueError("train_file path is needed!")

            tf.gfile.MakeDirs(self.dir+self.config['output_dir'])

            sess_config = tf.ConfigProto(
                gpu_options=tf.GPUOptions(allow_growth=True),
                allow_soft_placement=True)

            distribution = tf.contrib.distribute.MirroredStrategy(num_gpus=self.num_gpu, auto_shard_dataset=True)

            run_config = tf.contrib.tpu.RunConfig(
                master=None,
                save_summary_steps=100,
                log_step_count_steps=100,
                model_dir=self.dir+self.config['output_dir'],
                save_checkpoints_steps=self.config['save_checkpoints_steps'],
                train_distribute=distribution,
                session_config=sess_config)

            train_examples = squad_core.read_squad_examples(
                input_file=train_file,
                is_training=train,
                version_2_with_negative=self.config['version_2_with_negative'])

            num_train_steps = int(len(train_examples) / (train_batch_size * self.num_gpu) * num_train_epochs)
            num_warmup_steps = int(num_train_steps * self.config['warmup_proportion'])

            rng = random.Random(12345)
            rng.shuffle(train_examples)
        elif not train:
            if predict_file is None:
                raise ValueError("dev_file path is needed!")

            tf.gfile.MakeDirs(self.dir+self.config['output_dir'])

            sess_config = tf.ConfigProto(
                gpu_options=tf.GPUOptions(allow_growth=True, visible_device_list="0"),
                allow_soft_placement=True)

            run_config = tf.contrib.tpu.RunConfig(
                master=None,
                save_summary_steps=100,
                log_step_count_steps=100,
                model_dir=self.dir+self.config['output_dir'],
                save_checkpoints_steps=self.config['save_checkpoints_steps'],
                session_config=sess_config)

        model_fn = squad_core.model_fn_builder(
            bert_config=bert_config,
            init_checkpoint=self.dir+self.config['init_checkpoint'],
            learning_rate=self.config['learning_rate'],
            num_train_steps=num_train_steps,
            num_warmup_steps=num_warmup_steps,
            use_tpu=False,
            use_one_hot_embeddings=False,
            vocab_size=len(open(self.dir+self.config['vocab_file']).read().split('\n')) - 1,
            model_type=model_type,
            freeze_layers=num_freeze)

        estimator = tf.estimator.Estimator(
            model_fn=model_fn,
            config=run_config,
            params=dict(batch_size=train_batch_size))

        if en:
            print('\n""" English Model Loaded! """\n')
        else:
            print('\n""" Korean Model Loaded! """\n')

        if train:
            tf.logging.info("***** Running training *****")
            tf.logging.info("  Num orig examples = %d", len(train_examples))

            train_data_dir = self.dir + self.config['vocab_file'][:-4] + '_train_data/' + \
                             train_file.split('/')[-1][:-5] + '/'
            tf.gfile.MakeDirs(train_data_dir)

            if "train.tf_record" not in os.listdir(train_data_dir):
                train_writer = squad_core.FeatureWriter(
                    filename=os.path.join(train_data_dir, "train.tf_record"),
                    is_training=train)

                squad_core.convert_examples_to_features(
                    examples=train_examples,
                    tokenizer=tokenizer,
                    max_seq_length=max_seq_length,
                    doc_stride=self.config['doc_stride'],
                    max_query_length=self.config['max_query_length'],
                    is_training=train,
                    output_fn=train_writer.process_feature,
                    version_2_with_negative=self.config['version_2_with_negative'])
                train_writer.close()

                tf.logging.info("  Num split examples = %d", train_writer.num_features)

            tf.logging.info("  Batch size = %d", train_batch_size)
            tf.logging.info("  Num steps = %d", num_train_steps)
            del train_examples

            train_input_fn = squad_core.input_fn_builder(
                input_file=train_data_dir+"train.tf_record",
                seq_length=max_seq_length,
                is_training=train,
                drop_remainder=train)

            if self.dir+self.config['eval_file'] is not None:
                eval_examples = squad_core.read_squad_examples(
                    input_file=self.dir+self.config['eval_file'], is_training=train,
                    version_2_with_negative=self.config['version_2_with_negative'])

                if "eval_features.pkl" not in os.listdir(train_data_dir):
                    eval_writer = squad_core.FeatureWriter(
                        filename=os.path.join(train_data_dir, "eval.tf_record"),
                        is_training=train)

                    eval_features = []

                    def append_feature(feature):
                        eval_features.append(feature)
                        eval_writer.process_feature(feature)

                    squad_core.convert_examples_to_features(
                        examples=eval_examples,
                        tokenizer=tokenizer,
                        max_seq_length=max_seq_length,
                        doc_stride=self.config['doc_stride'],
                        max_query_length=self.config['max_query_length'],
                        is_training=train,
                        output_fn=append_feature,
                        version_2_with_negative=self.config['version_2_with_negative'])
                    eval_writer.close()
                    pk.dump(eval_features, open(train_data_dir + 'eval_features.pkl', 'wb'))
                else:
                    eval_features = pk.load(open(train_data_dir + 'eval_features.pkl', 'rb'))

                tf.logging.info("***** Running evaluation *****")
                tf.logging.info("  Num orig examples = %d", len(eval_examples))
                tf.logging.info("  Num split examples = %d", len(eval_features))
                del eval_examples

                predict_input_fn = squad_core.input_fn_builder(
                    input_file=train_data_dir + "eval.tf_record",
                    seq_length=max_seq_length,
                    is_training=train,
                    drop_remainder=False)

                train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=num_train_steps)
                eval_spec = tf.estimator.EvalSpec(input_fn=predict_input_fn, throttle_secs=120)
                tf.estimator.train_and_evaluate(
                    estimator,
                    train_spec,
                    eval_spec)
            else:
                estimator.train(input_fn=train_input_fn, max_steps=num_train_steps)
        elif not train:
            dev_data_dir = self.dir + self.config['vocab_file'][:-4] + '_dev_data/' + \
                           predict_file.split('/')[-1][:-5] + '/'
            tf.gfile.MakeDirs(dev_data_dir)

            model_step = estimator.get_variable_value("global_step")

            eval_examples = squad_core.read_squad_examples(
                input_file=predict_file, is_training=train,
                version_2_with_negative=self.config['version_2_with_negative'])

            if "eval_features.pkl" not in os.listdir(dev_data_dir):
                eval_writer = squad_core.FeatureWriter(
                    filename=os.path.join(dev_data_dir, "eval.tf_record"),
                    is_training=train)

                eval_features = []

                def append_feature(feature):
                    eval_features.append(feature)
                    eval_writer.process_feature(feature)

                squad_core.convert_examples_to_features(
                    examples=eval_examples,
                    tokenizer=tokenizer,
                    max_seq_length=max_seq_length,
                    doc_stride=self.config['doc_stride'],
                    max_query_length=self.config['max_query_length'],
                    is_training=train,
                    output_fn=append_feature,
                    version_2_with_negative=self.config['version_2_with_negative'])
                eval_writer.close()
                pk.dump(eval_features, open(dev_data_dir+'eval_features.pkl', 'wb'))
            else:
                eval_features = pk.load(open(dev_data_dir+'eval_features.pkl', 'rb'))

            tf.logging.info("***** Running predictions *****")
            tf.logging.info("  Num orig examples = %d", len(eval_examples))
            tf.logging.info("  Num split examples = %d", len(eval_features))

            predict_input_fn = squad_core.input_fn_builder(
                input_file=dev_data_dir+"eval.tf_record",
                seq_length=max_seq_length,
                is_training=train,
                drop_remainder=train)

            RawResult = collections.namedtuple("RawResult",
                                               ["unique_id", "start_logits", "end_logits"])

            # If running eval on the TPU, you will need to specify the number of
            # steps.
            all_results = []
            for result in estimator.predict(
                    predict_input_fn, yield_single_examples=True):
                if len(all_results) % 1000 == 0:
                    tf.logging.info("Processing example: %d" % (len(all_results)))
                unique_id = int(result["unique_ids"])
                start_logits = [float(x) for x in result["start_logits"].flat]
                end_logits = [float(x) for x in result["end_logits"].flat]
                all_results.append(
                    RawResult(
                        unique_id=unique_id,
                        start_logits=start_logits,
                        end_logits=end_logits))

            output_prediction_file = os.path.join(self.dir+self.config['output_dir'], "predictions"+str(model_step)+".json")
            output_nbest_file = None
            # os.path.join(self.dir+self.config['output_dir'], "nbest_predictions"+str(model_step)+".json")
            output_null_log_odds_file = None
            # os.path.join(self.dir+self.config['output_dir'], "null_odds.json")

            squad_core.write_predictions(
                eval_examples, eval_features, all_results,
                self.config['n_best_size'], self.config['max_answer_length'],
                en, output_prediction_file,
                output_nbest_file, output_null_log_odds_file,
                version_2_with_negative=self.config['version_2_with_negative'])
            predict_file = '/home/msl/data/squad/demo3.txt'
            if predict_file.split('.')[-1] != 'json':
                dev_set = json.load(open('.'.join(predict_file.split('.')[:-1])+'.json'))
            else:
                dev_set = json.load(open(predict_file))
            pred_set = json.load(open(os.path.join(self.dir+self.config['output_dir'], "predictions"+str(model_step)+".json")))

            eval0 = evaluate_v1.evaluate(dev_set['data'], pred_set, en=en)
            # eval1 = evaluate_v1.evaluate(dev_set['data'], pred_set)

            print('f1: ' + str(eval0['f1']))
            # print(eval1)

            if "evaluations.txt" not in os.listdir(self.dir+self.config['output_dir']):
                f = open(self.dir+self.config['output_dir']+"evaluations.txt", 'w')
                f.write('step' + '\t'*3 + 'evaluation_socore' + '\n')
                # f.write(str(model_step) + '\t' + "em: %.4f," % eval0['exact_match'] + " f1: %.4f" % eval0['f1'] + '\n')
                f.write(str(model_step) + '\t' + " f1: %.4f" % eval0['f1'] + '\n')
                f.close()
            else:
                f = open(self.dir+self.config['output_dir'] + "evaluations.txt", 'a')
                # f.write(str(model_step) + '\t' + "em: %.4f," % eval0['exact_match'] + " f1: %.4f" % eval0['f1'] + '\n')
                f.write(str(model_step) + '\t' + " f1: %.4f" % eval0['f1'] + '\n')
                f.close()


if __name__ == '__main__':
    config = json.load(open(os.environ['MAUM_ROOT'] + '/trained/bert/config.json', 'r'))
    model = BertSquad(config, num_gpu=8)

    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--train_batch_size', type=int, default=8) # large batch size: 1
    parser.add_argument('-l', '--max_seq_length', type=int, default=384) # large length: 280
    parser.add_argument('-i', '--num_train_epochs', type=float, default=2.0)
    parser.add_argument('-t', '--train', type=bool, default=False)
    parser.add_argument('-e', '--en', type=bool, default=False)
    parser.add_argument('-m', '--model_type', type=int, default=3)
    parser.add_argument('-f', '--num_freeze', type=int, default=None)
    parser.add_argument('--train_file', type=str, default=None)
    parser.add_argument('--dev_file', type=str, default=None)
    args = parser.parse_args()

    model.runner(args.train_batch_size, args.max_seq_length, args.num_train_epochs,
                 args.train, args.en, args.model_type, args.num_freeze, args.train_file, args.dev_file)
