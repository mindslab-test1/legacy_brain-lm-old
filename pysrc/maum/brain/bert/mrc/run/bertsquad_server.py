from concurrent import futures

import logging
import time
import grpc
import argparse
import json
import sys, os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.mrc.run.inference import BertSquad as BS
from maum.brain.bert.mrc.core.transform_data_for_bert import transform_data
from maum.brain.bert.mrc.bs_pb2 import OutputText, Outputs
from maum.brain.bert.mrc.bs_pb2_grpc import BertSquadServicer
from maum.brain.bert.mrc.bs_pb2_grpc import add_BertSquadServicer_to_server


class BertSquad(BertSquadServicer):
    def __init__(self, lang):
        self.config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/config.json', 'r'))
        self.model = BS(self.config, lang)

    def Answer(self, msg, context):
        '''
        0: text, 1: prob, 2: start_index, 3: end_index
        '''
        input_data = transform_data([context.context for context in msg.contexts], msg.question)
        answers = self.model.infer(input_data, msg.top_k)
        output = [OutputText(passage_idx=answer['passage_idx'],
                             text=answer['text'],
                             prob=answer['probability'],
                             start_index=answer['start_index'],
                             end_index=answer['end_index']) for answer in answers]
        outputs = Outputs()
        outputs.answers.extend(output)

        return outputs


_ONE_DAY_IN_SECONDS = 60 * 60 * 24

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='bertsquad runner executor')
    print(parser.description)
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='en')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=35001)

    args = parser.parse_args()

    bertsqaud = BertSquad(args.lang)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_BertSquadServicer_to_server(bertsqaud, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('bertsqaud starting at 0.0.0.0:%d' % args.port)
    print('bertsqaud starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
