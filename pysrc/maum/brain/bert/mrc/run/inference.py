import tensorflow as tf
import numpy as np
import collections
import json
import os

from ..core import modeling
from ..core import tokenization
from ..core import squad_core


class BertSquad:
    def __init__(self, config, lang, init_checkpoint=None, device=None, is_training=False):
        """
        config: config file or config path
        lang: str, 'en' or 'ko'
        init_checkpoint: str, None(model path in config) or model path
        device: int, None(0) or gpu id
        is_training: bool
        """
        if device is None:
            device = 0
        os.environ['CUDA_VISIBLE_DEVICES'] = str(device)
        if type(config) == str:
            self.config = json.load(open(config, 'r'))[lang]
        else:
            self.config = config[lang]
        self.dir = os.environ['MAUM_ROOT']
        self.is_training = is_training
        self.bert_config = modeling.BertConfig.from_json_file(self.config['bert_config'])

        self.input_ids = tf.placeholder(tf.int32, shape=[None, self.config['max_seq_length']], name='input_ids')
        self.input_mask = tf.placeholder(tf.int32, shape=[None, self.config['max_seq_length']], name='input_mask')
        self.segment_ids = tf.placeholder(tf.int32, shape=[None, self.config['max_seq_length']], name='segment_ids')

        self.model_out = squad_core.create_model(self.bert_config, is_training, self.input_ids, self.input_mask,
                                                 self.segment_ids, use_one_hot_embeddings=False,
                                                 model_type=self.config['model_type'])

        tvars = tf.trainable_variables()

        initialized_variable_names = {}
        if init_checkpoint is None:
            init_checkpoint = self.config['init_checkpoint']
        init_checkpoint = self.dir + init_checkpoint
        if init_checkpoint:
            (assignment_map, initialized_variable_names) = \
                modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
            tf.train.init_from_checkpoint(init_checkpoint, assignment_map)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        self.sess.run(tf.global_variables_initializer())
        self.sess.run(self.model_out,
                      feed_dict={self.input_ids: [[0 for _ in range(self.input_ids.shape[-1])]],
                                 self.input_mask: [[0 for _ in range(self.input_mask.shape[-1])]],
                                 self.segment_ids: [[0 for _ in range(self.segment_ids.shape[-1])]]})

        tf.gfile.MakeDirs(self.dir + self.config['output_dir'])
        self.tokenizer = tokenization.FullTokenizer(vocab_file=self.dir + self.config['vocab_file'],
                                                    do_lower_case=self.config['do_lower_case'])

    def infer(self, json_input, top_k):
        eval_examples = squad_core.read_squad_examples(input_file=json_input, is_training=False,
                                                       version_2_with_negative=self.config['version_2_with_negative'])

        eval_writer = squad_core.FeatureWriter(
            filename=os.path.join(self.dir + self.config['output_dir'], "eval.tf_record"),
            is_training=self.is_training)

        eval_features = []

        def append_feature(feature):
            eval_features.append(feature)
            eval_writer.process_feature(feature)

        squad_core.convert_examples_to_features(examples=eval_examples,
                                                tokenizer=self.tokenizer,
                                                max_seq_length=self.config['max_seq_length'],
                                                doc_stride=self.config['doc_stride'],
                                                max_query_length=self.config['max_query_length'],
                                                is_training=self.is_training,
                                                output_fn=append_feature,
                                                version_2_with_negative=self.config['version_2_with_negative'])
        eval_writer.close()

        feed_dict = {self.input_ids: [features.input_ids for features in eval_features],
                     self.input_mask: [features.input_mask for features in eval_features],
                     self.segment_ids: [features.segment_ids for features in eval_features]}
        logits = self.sess.run(self.model_out, feed_dict=feed_dict)
        
        RawResult = collections.namedtuple("RawResult", ["unique_id", "start_logits", "end_logits"])
        all_results = []
        for i in range(len(eval_features)):
            all_results.append(RawResult(eval_features[i].unique_id, logits[0][i], logits[1][i]))

        answers = squad_core.write_predictions(all_examples=eval_examples,
                                               all_features=eval_features,
                                               all_results=all_results,
                                               n_best_size=self.config['n_best_size'],
                                               max_answer_length=self.config['max_answer_length'],
                                               do_lower_case=self.config['do_lower_case'],
                                               version_2_with_negative=self.config['version_2_with_negative'])

        answers_list = []
        noanswers_list = []
        answers_scores = []
        for i in answers:
            batch_scores = []
            for ans in answers[i]:
                ans.update({'passage_idx': int(i)})
                if ans['text'] == '':
                    ans['text'] = '정답 없음'
                batch_scores.append(-1 * ans['probability'])
            if answers[i][np.argsort(batch_scores)[0]]['text'] == '정답 없음':
                noanswers_list.append(answers[i][np.argsort(batch_scores)[0]])
            else:
                for j in np.argsort(batch_scores)[:top_k]:
                    answers_list.append(answers[i][j])
                    answers_scores.append(-1 * answers[i][j]['probability'])
        out = [answers_list[i] for i in np.argsort(answers_scores)[:top_k]]+noanswers_list

        return out


if __name__ == '__main__':
    from ..core.transform_data_for_bert import transform_data

    config_path = os.environ['MAUM_ROOT'] + '/trained/bert/config.json'
    config = json.load(open(config_path, 'r'))
    model = BertSquad(config, 'ko')

    context = ["김영란법(부정청탁 및 금품수수 금지법)시행 이후 맞은 첫 주말인 지난 1일 오후 6시쯤, "
               "기자는 서울 서초구 G공익신고학원을 찾아갔다. 김영란법 위반자를 적발해 포상금을 받는 "
               "방법을 알려 주는 일명 란파라치(김영란법+파파라치) 양성 학원이다. 교육 수강생 이모(56자영업)씨와 "
               "박모(57퇴직자)씨가 다소 긴장한 표정으로 앉아 있었다.두 사람은 최근 이 학원에서 김영란법 위반자를 "
               "손쉽게 단속할 수 있는 비법 등 이론 교육(총 3시간30분)을 받은 수강생이다. 이들은 이날 서울의 한 병원 "
               "장례식장으로 첫 현장 출동을 앞둔 상황이었다.이 학원의 문성옥 대표가 몰래카메라 사용법과 현장활동 "
               "주의사항을 전달했다. 몰래카메라는 소매 끝 안쪽에 숨겨 촬영이 가능한 형태였다. 이 학원 강의실 한쪽 "
               "장식장 안에는 안경부터 모자라이터명함지갑 등 다양한 형태의 몰래카메라가 진열돼 있었다. 학원 관계자들은 "
               "최근 일부 란파라치 학원이 수강생들에게 몰래카메라를 시중가격보다 비싸게 팔았다는 비판 보도를 의식한 "
               "때문인지 가격은 철저히 함구했다."]
    question = ["김영란법이 뭐야"]

    json_input = transform_data(context, question)
    answer = model.infer(json_input, top_k=1)
    print(context)
    print(question)
    print(answer[0])
