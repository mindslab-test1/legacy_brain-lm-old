def transform_data(context, question):
    json_form = {'version': None, 'data': []}

    if type(context) == str:
        context = [context]
    else:
        context = context

    if type(question) == str:
        question = [question]
    else:
        question = question

    if len(question) > 1:
        for i in range(len(question)):
            form = {'title': None,
                    'paragraphs': [{'context': None,
                                    'qas': [{'id': "%d" % i,
                                             'question': question[i],
                                             'answers': [{'text': None,
                                                          'answer_start': None}]}]}]}
            try:
                form['paragraphs'][0]['context'] = context[i]
            except:
                form['paragraphs'][0]['context'] = context[0]

            json_form['data'].append(form)
    elif len(context) > 1:
        for i in range(len(context)):
            form = {'title': None,
                    'paragraphs': [{'context': context[i],
                                    'qas': [{'id': "%d" % i,
                                             'question': None,
                                             'answers': [{'text': None,
                                                          'answer_start': None}]}]}]}
            try:
                form['paragraphs'][0]['qas'][0]['question'] = question[i]
            except:
                form['paragraphs'][0]['qas'][0]['question'] = question[0]

            json_form['data'].append(form)
    else:
        form = {'title': None,
                'paragraphs': [{'context': context[0],
                                'qas': [{'id': 0,
                                         'question': None,
                                         'answers': [{'text': None,
                                                      'answer_start': None}]}]}]}
        form['paragraphs'][0]['qas'][0]['question'] = question[0]
        json_form['data'].append(form)
    out = json_form['data']

    return out


if __name__ == '__main__':
    context = "Greenhouse gas emissions worldwide are growing at an accelerating pace this year, " \
              "researchers said Wednesday, putting the world on track to face some of the most severe consequences of " \
              "global warming sooner than expected. Scientists described the quickening rate of carbon dioxide emissions " \
              "in stark terms, comparing it to a“speeding freight train” and laying part of the blame on an unexpected " \
              "surge in the appetite for oil as people around the world not only buy more cars but also drive them " \
              "farther than in the past — more than offsetting any gains from the spread of electric vehicles." \
              "“We’ve seen oil use go up five years in a row,” said Rob Jackson, a professor of earth system science " \
              "at Stanford and an author of one of two studies published Wednesday. “That’s really surprising.” Worldwide, " \
              "carbon emissions are expected to increase by 2.7 percent in 2018, according to the new research, " \
              "which was published by the Global Carbon Project, a group of 100 scientists from more than 50 academic " \
              "and research institutions and one of the few organizations to comprehensively examine global emissions numbers. " \
              "Emissions rose 1.6 percent last year, the researchers said, ending a three-year plateau.)"
    question = "Who is Rob Jackson?"
    contexts = ["Greenhouse gas emissions worldwide are growing at an accelerating pace this year, researchers said "
                "Wednesday, putting the world on track to face some of the most severe consequences of global warming "
                "sooner than expected. Scientists described the quickening rate of carbon dioxide emissions in stark "
                "terms, comparing it to a“speeding freight train” and laying part of the blame on an unexpected "
                "surge in the appetite for oil as people around the world not only buy more cars but also drive them "
                "farther than in the past — more than offsetting any gains from the spread of electric vehicles.",
                "“We’ve seen oil use go up five years in a row,” said Rob Jackson, a professor of earth system "
                "science at Stanford and an author of one of two studies published Wednesday. “That’s really "
                "surprising.”Worldwide, carbon emissions are expected to increase by 2.7 percent in 2018, according "
                "to the new research, which was published by the Global Carbon Project, a group of 100 scientists "
                "from more than 50 academic and research institutions and one of the few organizations to "
                "comprehensively examine global emissions numbers. Emissions rose 1.6 percent last year, "
                "the researchers said, ending a three-year plateau.)"]
    questions = ["What did researchers say?", "Who is Rob Jackson?"]

    json_input = transform_data(context, question)
    print(json_input)
    json_input = transform_data(contexts, questions)
    print(json_input)
