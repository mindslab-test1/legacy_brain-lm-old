from __future__ import print_function

import grpc
import argparse
import sys, os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.mrc.bs_pb2 import InputText, Context
from maum.brain.bert.mrc.bs_pb2_grpc import BertSquadStub


def ans(stub, input_context, input_question, top_k):
    if type(input_context) == str:
        input_context = [input_context]
    contexts = [Context(context=context) for context in input_context]
    inputs = InputText()
    inputs.contexts.extend(contexts)
    inputs.question = input_question
    inputs.top_k = top_k
    return stub.Answer(inputs)


def run(context, question, top_k, host, port):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = BertSquadStub(channel)
        return ans(stub, context, question, top_k)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=35001)
    parser.add_argument('-p', '--passage', type=str, default=["Greenhouse gas emissions worldwide are growing "
                                                             "at an accelerating pace this year, researchers said "
                                                             "Wednesday, putting the world on track to face some "
                                                             "of the most severe consequences of global warming sooner "
                                                             "than expected. Scientists described the quickening rate "
                                                             "of carbon dioxide emissions in stark terms, comparing it "
                                                             "to a“speeding freight train” and laying part of the "
                                                             "blame on an unexpected surge in the appetite for oil as"
                                                             " people around the world not only buy more cars but also"
                                                             " drive them farther than in the past — more than offset"
                                                             "ting any gains from the spread of electric vehicles."
                                                             "“We’ve seen oil use go up five years in a row,” said "
                                                             "Rob Jackson, a professor of earth system science "
                                                             "at Stanford and an author of one of two studies published"
                                                             " Wednesday. “That’s really surprising.” Worldwide, "
                                                             "carbon emissions are expected to increase by 2.7 percent"
                                                             " in 2018, according to the new research, which was "
                                                             "published by the Global Carbon Project, a group of 100"
                                                             " scientists from more than 50 academic and research"
                                                             " institutions and one of the few organizations to "
                                                             "comprehensively examine global emissions numbers. "
                                                             "Emissions rose 1.6 percent last year, the researchers "
                                                             "said, ending a three-year plateau.)",
                                                              "Greenhouse gas emissions worldwide are growing "
                                                             "at an accelerating pace this year, researchers said "
                                                             "Wednesday, putting the world on track to face some "
                                                             "of the most severe consequences of global warming sooner "
                                                             "than expected. Scientists described the quickening rate "
                                                             "of carbon dioxide emissions in stark terms, comparing it "
                                                             "to a“speeding freight train” and laying part of the "
                                                             "blame on an unexpected surge in the appetite for oil as"
                                                             " people around the world not only buy more cars but also"
                                                             " drive them farther than in the past — more than offset"
                                                             "ting any gains from the spread of electric vehicles."
                                                             "“We’ve seen oil use go up five years in a row,” said "
                                                             "Rob Jackson, a professor of earth system science "
                                                             "at Stanford and an author of one of two studies published"
                                                             " Wednesday. “That’s really surprising.” Worldwide, "
                                                             "carbon emissions are expected to increase by 2.7 percent"
                                                             " in 2018, according to the new research, which was "
                                                             "published by the Global Carbon Project, a group of 100"
                                                             " scientists from more than 50 academic and research"
                                                             " institutions and one of the few organizations to "
                                                             "comprehensively examine global emissions numbers. "
                                                             "Emissions rose 1.6 percent last year, the researchers "
                                                             "said, ending a three-year plateau.)"])
    parser.add_argument('-q', '--question', type=str, default="Who is Rob Jackson?")
    parser.add_argument('-k', '--top_k', type=int, default=1)
    args = parser.parse_args()
    print(args.question, args.host, args.port, args.top_k)

    for answer in run(args.passage, args.question, args.top_k, args.host, args.port).answers:
        print('passage_idx: %i\nanswer: %s\nprob: %.4f\nstart_idx: %i\nend_idx: %i\n'
              % (answer.passage_idx, answer.text, answer.prob, answer.start_index, answer.end_index))

