import json
import sys
import os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.mrc.run.inference import BertSquad as mrc
from maum.brain.bert.mrc.core.transform_data_for_bert import transform_data


def test_en_bertmrc():
    config = json.load(open(os.environ['MAUM_ROOT']+'/trained/bert/config.json', 'r'))
    model = mrc(config, 'en')

    long_context = ['The "freedom to provide services" under TFEU article 56 applies to people who give services '
                    '"for remuneration", especially commercial or professional activity. For example, '
                    'in Van Binsbergen v Bestuur van de Bedrijfvereniging voor de Metaalnijverheid a Dutch lawyer '
                    'moved to Belgium while advising a client in a social security case, and was told he could not '
                    'continue because Dutch law said only people established in the Netherlands could give legal advice. '
                    'The Court of Justice held that the freedom to provide services applied, it was directly effective, '
                    'and the rule was probably unjustified: having an address in the member state would be enough '
                    'to pursue the legitimate aim of good administration of justice. The Court of Justice has held '
                    'that secondary education falls outside the scope of article 56, because usually the state funds it, '
                    'though higher education does not. Health care generally counts as a service. '
                    'In Geraets-Smits v Stichting Ziekenfonds Mrs Geraets-Smits claimed she should be reimbursed '
                    'by Dutch social insurance for costs of receiving treatment in Germany. '
                    'The Dutch health authorities regarded the treatment unnecessary, so she argued this restricted '
                    'the freedom (of the German health clinic) to provide services. Several governments submitted '
                    'that hospital services should not be regarded as economic, and should not fall within article 56. '
                    'But the Court of Justice held health was a "service" even though the government '
                    '(rather than the service recipient) paid for the service. National authorities could be justified '
                    'in refusing to reimburse patients for medical services abroad if the health care received at home '
                    'was without undue delay, and it followed "international medical science" on which treatments counted '
                    'as normal and necessary. The Court requires that the individual circumstances of a patient justify '
                    'waiting lists, and this is also true in the context of the UK\'s National Health Service. Aside '
                    'from public services, another sensitive field of services are those classified as illegal. '
                    'Josemans v Burgemeester van Maastricht held that the Netherlands\' regulation of cannabis consumption,'
                    ' including the prohibitions by some municipalities on tourists (but not Dutch nationals) going '
                    'to coffee shops, fell outside article 56 altogether. The Court of Justice reasoned that narcotic '
                    'drugs were controlled in all member states, and so this differed from other cases where prostitution '
                    'or other quasi-legal activity was subject to restriction. If an activity does fall within article 56, '
                    'a restriction can be justified under article 52 or overriding requirements developed by '
                    'the Court of Justice. In Alpine Investments BV v Minister van Financiën a business that '
                    'sold commodities futures (with Merrill Lynch and another banking firms) attempted to challenge '
                    'a Dutch law that prohibiting cold calling customers. The Court of Justice held the Dutch '
                    'prohibition pursued a legitimate aim to prevent "undesirable developments in securities trading" '
                    'including protecting the consumer from aggressive sales tactics, thus maintaining confidence '
                    'in the Dutch markets. In Omega Spielhallen GmbH v Bonn a "laserdrome" business was banned by '
                    'the Bonn council. It bought fake laser gun services from a UK firm called Pulsar Ltd, '
                    'but residents had protested against "playing at killing" entertainment. The Court of Justice held '
                    'that the German constitutional value of human dignity, which underpinned the ban, '
                    'did count as a justified restriction on freedom to provide services. In Liga Portuguesa '
                    'de Futebol v Santa Casa da Misericórdia de Lisboa the Court of Justice also held that '
                    'the state monopoly on gambling, and a penalty for a Gibraltar firm that had sold internet '
                    'gambling services, was justified to prevent fraud and gambling where people\'s views were highly '
                    'divergent. The ban was proportionate as this was an appropriate and necessary way to tackle '
                    'the serious problems of fraud that arise over the internet. In the Services Directive a group of '
                    'justifications were codified in article 16 that the case law has developed.']
    long_question = ["Why was the Dutch lawyer who moved to Belgium while advising a client "
                     "in a social society case told he couldn't continue?"]

    mid_context = ["Greenhouse gas emissions worldwide are growing at an accelerating pace this year, researchers said "
               "Wednesday, putting the world on track to face some of the most severe consequences of global warming "
               "sooner than expected. Scientists described the quickening rate of carbon dioxide emissions in stark "
               "terms, comparing it to a“speeding freight train” and laying part of the blame on an unexpected "
               "surge in the appetite for oil as people around the world not only buy more cars but also drive them "
               "farther than in the past — more than offsetting any gains from the spread of electric vehicles."
               "“We’ve seen oil use go up five years in a row,” said Rob Jackson, a professor of earth system "
               "science at Stanford and an author of one of two studies published Wednesday. “That’s really "
               "surprising.”Worldwide, carbon emissions are expected to increase by 2.7 percent in 2018, according to "
               "the new research, which was published by the Global Carbon Project, a group of 100 scientists from "
               "more than 50 academic and research institutions and one of the few organizations to comprehensively "
               "examine global emissions numbers. Emissions rose 1.6 percent last year, the researchers said, ending a "
               "three-year plateau.)"]
    mid_question = ["Who is Rob Jackson?"]

    short_context = ['On 7 January 1900, Tesla left Colorado Springs.[citation needed] His lab was torn down in 1904, '
                     'and its contents were sold two years later to satisfy a debt.']
    short_question = ['When did Tesla depart from Colorado Springs?']

    en_long_out = open('test_en_long_out.txt').read()
    en_mid_out = open('test_en_mid_out.txt').read()
    en_short_out = open('test_en_short_out.txt').read()

    assert str(model.infer(transform_data(long_context, long_question))) == en_long_out
    assert str(model.infer(transform_data(mid_context, mid_question))) == en_mid_out
    assert str(model.infer(transform_data(short_context, short_question))) == en_short_out
