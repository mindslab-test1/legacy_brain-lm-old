from __future__ import print_function

import grpc
import argparse
import sys, os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.bert.exsumm import bex_pb2
from maum.brain.bert.exsumm import bex_pb2_grpc


def exsumm(stub, title, body):
    return stub.ExSummary(bex_pb2.InputText(title=title, body=body))


def run(title, body, host, port):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = bex_pb2_grpc.BertExSummStub(channel)
        return exsumm(stub, title, body)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=35004)
    parser.add_argument('-t', '--title', type=str, default='송송커플 이혼하자 블러썸엠앤씨·스튜디오드래곤 주가 \'화들짝\'')
    parser.add_argument('-b', '--body', type=str, default='''송송커플(배우 송중기·송혜교)의 이혼 소식에 스튜디오드래곤과 블러썸엠앤씨 등 상장사 주가도 '화들짝' 놀란 모습이다. 
    블러썸엠앤씨는 배우 송중기의 소속사(블러썸엔터테인먼트) 대표가 임원으로 재직 중인 곳이고, 스튜디오드래곤은 송중기가 주연인 아스달연대기를 제작한 곳이다. 
    27일 이날 오전 11시 23분 현재 코스닥 시장에서 블러썸엠앤씨는 전날보다 100원(0.4%) 하락한 2만4900원에 거래되고 있다. 장중 2만4300원까지 하락하기도 했다. 
    주가가 하락하는 것은 블러썸엠앤씨가 잇달아 영상, 영화, 방송프로그램 제작 업체 등의 주식을 양수한데 따른 것으로 풀이된다. 
    블러썸엠앤씨는 전날 방송프로그램 제작 및 판매업을 영위하는 주식회사 아이비스포츠의 주식 12만주를 60억원에 인수했다. 
    앞서 11일에는 영상콘텐츠를 제작하는 블러썸스토리 주식 100만주를 100억원에 인수했고 영화 및 영상물제작, 영화수입 배급업 등을 영위하는 블러썸픽쳐스 주식 1만1111주도 150억원에 사들였다. 
    특히 블러썸엠앤씨의 지영주 대표이사와 주방옥 이사는 송중기 씨의 소속사인 블러썸엔터테인먼트의 공동대표다. 
    블러썸엔터테인먼트도 블러썸앰엔씨에 합류할 가능성이 높다는 의미다. 
    다만 블러썸엠앤씨와 블러썸엔터테인먼트는 실제 지분 거래나 사업 제휴 등의 움직임은 보이지 않고 있다.
    스튜디오드래곤도 관심사다. 스튜디오드래곤은 '아스달연대기'를 앞세워 흥행을 노리고 있다. 아스달연대기에는 송중기가 출연한다. 
    이날 오전 11시 23분 기준 코스닥 시장에서 스튜디오드래곤은 전날보다 900원(1.29%) 내린 6만8800원에 거래되고 있다. 
    주가의 부진은 아스달연대기가 기대에 미치지 못했다는 평가가 우세하기 때문이라는 설명이다. 송중기의 이혼과는 별개라는 의미다. 
    이기훈 하나금융투자 연구원은 "최근 주가 하락은 아스달연대기에 대한 실망감에서 비롯됐지만 하반기에는 작품들의 제작비 안정화 등으로 수익성이 점진적으로 상향될 것"이라고 봤다. 
    엔터업계 관계자는 "송중기의 이혼은 사생활이기 때문에 아스달연대기에 직접적인 영향을 주긴 어려울 것"이라며 "심지어 송중기의 출연분은 이미 촬영도 마친 상태"라고 귀띔했다.''')
    args = parser.parse_args()

    outs = run(args.title, args.body, args.host, args.port)
    print('== extractive summary ==')
    for exsent in outs.exsents:
        print(exsent.sent)
    print('======= keywords =======')
    print([exword.word for exword in outs.exwords])
