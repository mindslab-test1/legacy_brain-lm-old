from __future__ import print_function

import grpc
import argparse

import gpt_pb2
import gpt_pb2_grpc


def get_ans(stub, input_context):
    return stub.Gen(gpt_pb2.InputText(context=input_context))


def run(context, host, port):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = gpt_pb2_grpc.GptTextGenStub(channel)
        return get_ans(stub, context)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=35003)
    parser.add_argument('-p', '--passage', type=str, default="As reported by ESPN's Michelle Beadle on Around the NFL on Sunday,")
    args = parser.parse_args()

    print(run(args.passage, args.host, args.port).gentext)
