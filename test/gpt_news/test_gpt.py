#!/usr/bin/env python3

import argparse
import json
import os
import numpy as np
import tensorflow as tf
from core import model, sample, encoder
import time
import tqdm
sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from maum.brain.gpt.gpt_news.run.gpt_inference import GPT

def test_gpt():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model_name_or_path', type=str,
                        default='/workspaces/venv_bert/gpt-2/core/models/cnn_category_1024',
                        help='pretrained model name or path to local checkpoint')
    parser.add_argument("--device", type=int, default=0)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--nsamples", type=int, default=1)
    parser.add_argument("--batch_size", type=int, default=1)
    parser.add_argument("--length", type=int, default=None)
    parser.add_argument("--temperature", type=int, default=1)
    parser.add_argument("--top_k", type=int, default=0)
    parser.add_argument("--top_p", type=float, default=.0)
    parser.add_argument('--conditional', type=bool, default=True)
    args = parser.parse_args()

    input1 = "As reported by ESPN's Michelle Beadle on Around the NFL on Sunday,"
    input2 = "Like China, which is locked in a full-blown trade war with the United States,"
    input3 = "But it doesn't have the same firepower as neighboring China, the world's second largest economy,"

    output_1 = open("./gpt_output1.txt", 'r').read()
    output_2 = open("./gpt_output2.txt", 'r').read()
    output_3 = open("./gpt_output3.txt", 'r').read()

    model = GPT(args.model_name_or_path,
                args.device,
                args.conditional,
                args.seed,
                args.nsamples,
                args.batch_size,
                args.length,
                args.temperature,
                args.top_k,
                args.top_p)

    out1 = model.infer(input1)
    out2 = model.infer(input2)
    out3 = model.infer(input3)

    assert(output_1 == out1[0])
    assert(output_2 == out2[0])
    assert(output_3 == out3[0])

