#!/usr/bin/env python3

import json
import os
import numpy as np
import tensorflow as tf
from core import model, sample, encoder
import time
import tqdm
sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from maum.brain.gpt.gpt_news.run.gpt_inference import GPT

def test_sort_text(benchmark):

    model_name = '/workspaces/venv_bert/gpt-2/core/models/cnn_category_1024'
    device = 0
    seed = 0
    nsamples = 1
    batch_size = 1
    length = None
    temperature = 1
    top_k = 0
    top_p = .0
    conditional = True

    input1 = "As reported by ESPN's Michelle Beadle on Around the NFL on Sunday,"

    model = GPT(model_name,
                device,
                conditional,
                seed,
                nsamples,
                batch_size,
                length,
                temperature,
                top_k,
                top_p)

    benchmark(model.infer, input1)

def test_middle_text(benchmark):

    model_name = '/workspaces/venv_bert/gpt-2/core/models/cnn_category_1024'
    device = 0
    seed = 0
    nsamples = 1
    batch_size = 1
    length = None
    temperature = 1
    top_k = 0
    top_p = .0
    conditional = True

    input2 = "Like China, which is locked in a full-blown trade war with the United States,"

    model = GPT(model_name,
                device,
                conditional,
                seed,
                nsamples,
                batch_size,
                length,
                temperature,
                top_k,
                top_p)

    benchmark(model.infer, input2)

def test_long_text(benchmark):

    model_name = '/workspaces/venv_bert/gpt-2/core/models/cnn_category_1024'
    device = 0
    seed = 0
    nsamples = 1
    batch_size = 1
    length = None
    temperature = 1
    top_k = 0
    top_p = .0
    conditional = True

    input3 = "But it doesn't have the same firepower as neighboring China, the world's second largest economy,"

    model = GPT(model_name,
                device,
                conditional,
                seed,
                nsamples,
                batch_size,
                length,
                temperature,
                top_k,
                top_p)

    benchmark(model.infer, input3)

