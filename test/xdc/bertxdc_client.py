from __future__ import print_function

import grpc
import argparse

from maum.brain.bert.xdc import bx_pb2
from maum.brain.bert.xdc import bx_pb2_grpc


def get_ans(stub, input_context):
    return stub.Answer(bx_pb2.InputText(context=input_context))


def run(context, host, port):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = bx_pb2_grpc.BertXDCStub(channel)
        return get_ans(stub, context)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=35003)
    parser.add_argument('-p', '--passage', type=str, default=
                        "전주맛집 중 빠질 수 없는 전주 먹거리는 바로 막걸리다. "
                        "전주에서 막걸리 하면 상다리가 휠만큼 많은 안주들을 맛볼 수 있으며 푸짐한 인심으로도 유명하다. "
                        "그중 맛은 물론 합리적인 가격과 분위기, 볼거리가 많은 \"한옥전주막걸리\"는 "
                        "넉넉한 인심과 친절한 서비스를 자랑한다. "
                        "가족과의 외식, 회식장소로도 많은 관심을 받고 있는 \"한옥전주막걸리\"는 "
                        "전체 인원 200명 수용 가능한 내부 크기와 야외 테라스 및 최신 트렌드에 걸맞는 젊은층을 "
                        "겨냥한 인테리어를 자랑하고, 10가지 이상의 기본 안주는 물론, 메인 안주에도 노력을 쏟아 "
                        "방문객들의 재방문이 줄을 잇고 있다. 매일 시간마다 다른 구성으로 즐길 수 있는 색다른 라이브 공연으로 "
                        "눈과 귀를 만족시켜주는 장점을 갖춰 다른 막걸리집과 차별함을 두고 있으며, 다양한 볼거리가 많은 "
                        "전주한옥마을에서 5분 정도의 가까운 거리에 위치해있어 전주를 찾는 관광객들에게도 많은 관심을 받고 있다. "
                        "젊은층에게도 인기 있는 \"한옥전주막걸리\"는 언제나 건강한 재료를 사용하고 남녀노소 즐길 수 있는 "
                        "막걸리를 널리 알리기 위해 전 직원이 노력하고 있다고 관계자는 밝혔다. 전라북도 블로거들이 적극 추천하는 "
                        "인증 먹거리 \"한옥전주막걸리\"는 전주 우아동에 위치하고 있으며 오후 4시부터 오전 2시까지 영업하고 있다. "
                        "공연 및 단체 예약문의는 063-241-4466으로 가능하다.")
    args = parser.parse_args()
    
    outs = run(args.passage, args.host, args.port)
    for out in outs.labels:
        print('label: %s - prob: %.6f' % (out.label, out.prob))
    for i, out in enumerate(outs.sent_idxes):
        print('sent_idxes_%i' % i)
        print(' start:', out.start)
        print(' end:', out.end)
        print(' weight: %.6f' % out.weight)
    for i, out in enumerate(outs.word_idxes):
        print('word_idxes_%i' % i)
        print(' start:', out.start)
        print(' end:', out.end)
        print(' weight: %.6f' % out.weight)

